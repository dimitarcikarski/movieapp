package mk.codeacademy.movieapp.interfaces;

import mk.codeacademy.movieapp.model.db_models.FavMovie;
import mk.codeacademy.movieapp.model.db_models.FavTvShows;
import mk.codeacademy.movieapp.model.db_models.Watchlist;

public interface OnFavMovieClick {
    void onMovieClick(FavMovie favMovie);
    void onTvClick(FavTvShows tvShows);
}
