package mk.codeacademy.movieapp.interfaces;

import mk.codeacademy.movieapp.model.db_models.Watchlist;

public interface OnWatchlistClick {
    void onWatchlistClick(Watchlist watchlist);
}
