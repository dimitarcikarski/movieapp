package mk.codeacademy.movieapp.interfaces;

import mk.codeacademy.movieapp.model.discover_movie.MovieResults;

public interface OnClickSeeAll {
    void onClickSeeAll(MovieResults movieResults);
}
