package mk.codeacademy.movieapp.interfaces;

import mk.codeacademy.movieapp.model.db_models.RecentSearch;
import mk.codeacademy.movieapp.model.search.SearchResults;

public interface OnSearchItemClick {
    void onItemClick(SearchResults results);
    void onRecentSearchClick(RecentSearch recentSearch);
}
