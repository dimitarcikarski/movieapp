package mk.codeacademy.movieapp.interfaces;

import mk.codeacademy.movieapp.model.discover_movie.MediaResponseModel;
import mk.codeacademy.movieapp.model.discover_movie.MovieResults;
import mk.codeacademy.movieapp.model.movie_credits.KnownForCast;

public interface OnMediaItemClick {
    void onItemClick(MovieResults movie);
    void onKnownForClick(KnownForCast knownForCast);
    void onSeeAllClick(MediaResponseModel mediaResponseModel , int mediaType);
}
