package mk.codeacademy.movieapp.interfaces;

import mk.codeacademy.movieapp.model.people.PeopleResult;

public interface OnPersonClick {
    void onPersonClick(PeopleResult peopleResult);
}
