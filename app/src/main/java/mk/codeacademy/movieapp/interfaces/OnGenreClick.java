package mk.codeacademy.movieapp.interfaces;

import mk.codeacademy.movieapp.model.movie_details.Genre;

public interface OnGenreClick {
    void onGenreClick(Genre genre);
}
