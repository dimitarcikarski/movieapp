package mk.codeacademy.movieapp.interfaces;

import mk.codeacademy.movieapp.model.movie_credits.Cast;
import mk.codeacademy.movieapp.model.movie_credits.Crew;

public interface OnCastClick {
    void onCastClick(Cast cast);
    void onCrewClick(Crew crew);
}
