package mk.codeacademy.movieapp.interfaces;

import mk.codeacademy.movieapp.model.discover_movie.MovieResults;

public interface OnSliderClick {

    void onVideoClick(MovieResults movieResults);
}
