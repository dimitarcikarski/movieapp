package mk.codeacademy.movieapp.interfaces;

import mk.codeacademy.movieapp.model.videos.VideoResults;

public interface OnMediaClick {
    void onVideoClick(int position);
}
