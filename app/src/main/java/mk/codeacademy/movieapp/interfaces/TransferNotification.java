package mk.codeacademy.movieapp.interfaces;

public interface TransferNotification {
    void transferNotification(String text);
}
