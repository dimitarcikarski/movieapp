package mk.codeacademy.movieapp.interfaces;

import mk.codeacademy.movieapp.model.db_models.History;

public interface OnHistoryClick {
    void onHistoryClick(History history);
}
