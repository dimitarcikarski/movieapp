package mk.codeacademy.movieapp.interfaces;

public interface OnPictureClick {
    void onPictureClick(int position);
}
