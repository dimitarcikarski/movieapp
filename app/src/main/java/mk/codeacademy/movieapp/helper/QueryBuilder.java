package mk.codeacademy.movieapp.helper;

import mk.codeacademy.movieapp.adapter.SliderAdapter;
import mk.codeacademy.movieapp.common.Constants;
import okhttp3.HttpUrl;

public class QueryBuilder {

    public String moviesByFavouriteGenre(String genre , int page){

        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.themoviedb.org/3/discover/movie").newBuilder();

        urlBuilder.addQueryParameter("api_key" , Constants.API_key.API_KEY);
        urlBuilder.addQueryParameter("language" , "en-US");
        urlBuilder.addQueryParameter("sort_by","popularity.desc");
        urlBuilder.addQueryParameter("include_adult","false");
        urlBuilder.addQueryParameter("include_video","false");
        urlBuilder.addQueryParameter("page",page + "");
        urlBuilder.addQueryParameter("with_genres",genre);
        String url = urlBuilder.build().toString();

        return url;
    }

    public String movies(int sectionType , String page){

        String section = "";

        switch (sectionType){
            case Constants.MovieSectionType.IN_THEATHERS:
                section = "now_playing";
                break;
            case Constants.MovieSectionType.POPULAR:
                section = "popular";
                break;
            case Constants.MovieSectionType.UPCOMMING:
                section = "upcoming";
                break;
            case Constants.MovieSectionType.TOP_RATED:
                section = "top_rated";
                break;
        }

        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.themoviedb.org/3/movie/"+section).newBuilder();

        urlBuilder.addQueryParameter("api_key" , Constants.API_key.API_KEY);
        urlBuilder.addQueryParameter("language" , "en-US");
        urlBuilder.addQueryParameter("page",page);
        String url = urlBuilder.build().toString();

        return url;
    }

    public String tvShows(int sectionType , String page){

        String section = "";

        switch (sectionType){
            case Constants.TvSectionType.AIRING_TODAY:
                section = "airing_today";
                break;
            case Constants.TvSectionType.POPULAR:
                section = "popular";
                break;
            case Constants.TvSectionType.ON_THE_AIR:
                section = "on_the_air";
                break;
            case Constants.TvSectionType.TOP_RATED:
                section = "top_rated";
                break;
        }

        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.themoviedb.org/3/tv/"+section).newBuilder();

        urlBuilder.addQueryParameter("api_key" , Constants.API_key.API_KEY);
        urlBuilder.addQueryParameter("language" , "en-US");
        urlBuilder.addQueryParameter("page",page);
        String url = urlBuilder.build().toString();

        return url;
    }

    public String people(String page){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(" https://api.themoviedb.org/3/person/popular").newBuilder();

        urlBuilder.addQueryParameter("api_key" , Constants.API_key.API_KEY);
        urlBuilder.addQueryParameter("language" , "en-US");
        urlBuilder.addQueryParameter("page",page);
        urlBuilder.addQueryParameter("adult","false");
        String url = urlBuilder.build().toString();

        return url;

    }

    public String details(int id , int typeOfPicture){

        String type;
        if (typeOfPicture == Constants.TypeOfPicture.MOVIE){
            type = "movie";
        } else {
            type = "tv";
        }
        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.themoviedb.org/3/"+type+"/"+id).newBuilder();

        urlBuilder.addQueryParameter("api_key" , Constants.API_key.API_KEY);
        urlBuilder.addQueryParameter("language" , "en-US");
        String url = urlBuilder.build().toString();

        return url;
    }

    public String similar(int id , int typeOfPicture){
        String type;
        if (typeOfPicture == Constants.TypeOfPicture.MOVIE){
            type = "movie";
        } else {
            type = "tv";
        }
        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.themoviedb.org/3/"+type+"/"+id+"/similar").newBuilder();

        urlBuilder.addQueryParameter("api_key" , Constants.API_key.API_KEY);
        urlBuilder.addQueryParameter("language" , "en-US");
        urlBuilder.addQueryParameter("page","1");
        String url = urlBuilder.build().toString();

        return url;
    }

    public String cast(int id , int typeOfPicture){

        String type;
        if (typeOfPicture == Constants.TypeOfPicture.MOVIE){
            type = "movie";
        } else {
            type = "tv";
        }
        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.themoviedb.org/3/"+type+"/"+id+"/credits").newBuilder();

        urlBuilder.addQueryParameter("api_key" , Constants.API_key.API_KEY);
        String url = urlBuilder.build().toString();

        return url;
    }

    public String review(int id , int typeOfPicture){

        String type;
        if (typeOfPicture == Constants.TypeOfPicture.MOVIE){
            type = "movie";
        } else {
            type = "tv";
        }

        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.themoviedb.org/3/"+type+"/"+id+"/reviews").newBuilder();

        urlBuilder.addQueryParameter("api_key" , Constants.API_key.API_KEY);
        urlBuilder.addQueryParameter("language" , "en-US");
        urlBuilder.addQueryParameter("page","1");
        String url = urlBuilder.build().toString();

        return url;
    }

    public String person(int id){
        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.themoviedb.org/3/person/"+id).newBuilder();

        urlBuilder.addQueryParameter("api_key" , Constants.API_key.API_KEY);
        urlBuilder.addQueryParameter("language" , "en-US");
        String url = urlBuilder.build().toString();

        return url;
    }

    public String knownForCombined(int id){

        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.themoviedb.org/3/person/"+id+"/combined_credits").newBuilder();

        urlBuilder.addQueryParameter("api_key" , Constants.API_key.API_KEY);
        urlBuilder.addQueryParameter("language" , "en-US");
        String url = urlBuilder.build().toString();

        return url;

    }

    public String multiSearch(String query){

        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.themoviedb.org/3/search/multi").newBuilder();

        urlBuilder.addQueryParameter("api_key" , Constants.API_key.API_KEY);
        urlBuilder.addQueryParameter("query",query);
        urlBuilder.addQueryParameter("page","1");
        urlBuilder.addQueryParameter("include_adult","false");
        String url = urlBuilder.build().toString();

        return url;

    }

    public String randomSearchPeople(String query){

        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.themoviedb.org/3/search/person").newBuilder();

        urlBuilder.addQueryParameter("api_key" , Constants.API_key.API_KEY);
        urlBuilder.addQueryParameter("query",query);
        urlBuilder.addQueryParameter("page","1");
        urlBuilder.addQueryParameter("include_adult","false");
        String url = urlBuilder.build().toString();

        return url;

    }

    public String pictures(int id , int typeOfPicture){
        String type;
        if (typeOfPicture == Constants.TypeOfPicture.MOVIE){
            type = "movie";
        }else if (typeOfPicture == Constants.TypeOfPicture.PERSON) {
            type = "person";
        }else {
            type = "tv";
        }
        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.themoviedb.org/3/"+type+"/"+id+"/images").newBuilder();

        urlBuilder.addQueryParameter("api_key" , Constants.API_key.API_KEY);
        if (typeOfPicture == Constants.TypeOfPicture.MOVIE){
            urlBuilder.addQueryParameter("include_image_language","en");
        }
        String url = urlBuilder.build().toString();

        return url;
    }

    public String videos(int id , int typeOfPicture){
        String type;
        if (typeOfPicture == Constants.TypeOfPicture.MOVIE){
            type = "movie";
        } else {
            type = "tv";
        }
        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.themoviedb.org/3/"+type+"/"+id+"/videos").newBuilder();

        urlBuilder.addQueryParameter("api_key" , Constants.API_key.API_KEY);
        urlBuilder.addQueryParameter("language" , "en-US");

        return urlBuilder.build().toString();
    }

    public String frontGenre(){
        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.themoviedb.org/3/genre/movie/list").newBuilder();

        urlBuilder.addQueryParameter("api_key" , Constants.API_key.API_KEY);
        urlBuilder.addQueryParameter("language" , "en-US");

        return urlBuilder.build().toString();
    }

    //https://api.themoviedb.org/3/discover/movie?api_key=261eb855ea119759e11756f84891327f&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_genres=27

    public String genreDetail(String genre , String page){

        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.themoviedb.org/3/discover/movie").newBuilder();

        urlBuilder.addQueryParameter("api_key" , Constants.API_key.API_KEY);
        urlBuilder.addQueryParameter("language" , "en-US");
        urlBuilder.addQueryParameter("sort_by" , "popularity.desc");
        urlBuilder.addQueryParameter("include_adult" , "false");
        urlBuilder.addQueryParameter("page",page);
        urlBuilder.addQueryParameter("with_genres" , genre);

        return urlBuilder.build().toString();
    }

    public String oscarWinners(){

        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.themoviedb.org/3/list/28").newBuilder();

        urlBuilder.addQueryParameter("api_key" , Constants.API_key.API_KEY);
        urlBuilder.addQueryParameter("language" , "en-US");

        return urlBuilder.build().toString();
    }

    //https://api.themoviedb.org/3/trending/all/day?api_key=261eb855ea119759e11756f84891327f

    public String trending(){

        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.themoviedb.org/3/trending/all/day").newBuilder();

        urlBuilder.addQueryParameter("api_key" , Constants.API_key.API_KEY);

        return urlBuilder.build().toString();
    }
}
