package mk.codeacademy.movieapp.helper;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.adapter.CastAdapter;
import mk.codeacademy.movieapp.adapter.CrewAdapter;
import mk.codeacademy.movieapp.adapter.GenreAdapter;
import mk.codeacademy.movieapp.adapter.GenreFrontAdapter;
import mk.codeacademy.movieapp.adapter.KnownForAdapter;
import mk.codeacademy.movieapp.adapter.MovieItemAdapter;
import mk.codeacademy.movieapp.adapter.MovieSectionAdapter;
import mk.codeacademy.movieapp.adapter.PeopleAdapter;
import mk.codeacademy.movieapp.adapter.PictureAdapter;
import mk.codeacademy.movieapp.adapter.PictureListAdapter;
import mk.codeacademy.movieapp.adapter.ReviewsAdapter;
import mk.codeacademy.movieapp.adapter.SearchAdapter;
import mk.codeacademy.movieapp.adapter.SeasonsAdapter;
import mk.codeacademy.movieapp.adapter.SeeAllAdapter;
import mk.codeacademy.movieapp.adapter.SliderAdapter;
import mk.codeacademy.movieapp.adapter.VideoAdapter;
import mk.codeacademy.movieapp.adapter.VideoListAdapter;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnCastClick;
import mk.codeacademy.movieapp.interfaces.OnGenreClick;
import mk.codeacademy.movieapp.interfaces.OnMediaClick;
import mk.codeacademy.movieapp.interfaces.OnMediaItemClick;
import mk.codeacademy.movieapp.interfaces.OnPictureClick;
import mk.codeacademy.movieapp.interfaces.OnSearchItemClick;
import mk.codeacademy.movieapp.interfaces.OnSliderClick;
import mk.codeacademy.movieapp.interfaces.Transfer;
import mk.codeacademy.movieapp.interfaces.TransferNotification;
import mk.codeacademy.movieapp.interfaces.TransferPicture;
import mk.codeacademy.movieapp.model.db_models.History;
import mk.codeacademy.movieapp.model.discover_movie.MediaResponseModel;
import mk.codeacademy.movieapp.model.discover_movie.MovieResults;
import mk.codeacademy.movieapp.model.genres.GenreResponseModel;
import mk.codeacademy.movieapp.model.movie_credits.Cast;
import mk.codeacademy.movieapp.model.movie_credits.Crew;
import mk.codeacademy.movieapp.model.movie_credits.KnownForCast;
import mk.codeacademy.movieapp.model.movie_credits.KnowsForResponseModel;
import mk.codeacademy.movieapp.model.movie_credits.MovieCreditsResponseModel;
import mk.codeacademy.movieapp.model.movie_details.Genre;
import mk.codeacademy.movieapp.model.movie_details.Movie;
import mk.codeacademy.movieapp.model.oscar.OscarResponseModel;
import mk.codeacademy.movieapp.model.people.PeopleDetails;
import mk.codeacademy.movieapp.model.people.PeopleResponseModel;
import mk.codeacademy.movieapp.model.people.PeopleResult;
import mk.codeacademy.movieapp.model.pictures.Backdrop;
import mk.codeacademy.movieapp.model.pictures.PeopleImagesResponseModel;
import mk.codeacademy.movieapp.model.pictures.PictureResponseModel;
import mk.codeacademy.movieapp.model.reviews.ReviewResponseModel;
import mk.codeacademy.movieapp.model.reviews.ReviewResults;
import mk.codeacademy.movieapp.model.search.MultiSearchResponseModel;
import mk.codeacademy.movieapp.model.search.SearchResults;
import mk.codeacademy.movieapp.model.tv_details.Seasons;
import mk.codeacademy.movieapp.model.tv_details.Tv_details;
import mk.codeacademy.movieapp.model.videos.VideoResults;
import mk.codeacademy.movieapp.model.videos.VideosResponseModel;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Response;

public class Requests {


    Gson gson = new Gson();

    public void loadMoviesTvs(final LinearLayout linearLayout,
                              final ProgressBar progressBar,
                              final RecyclerView recyclerView ,
                              final ArrayList<MediaResponseModel> datalist ,
                              final Activity activity ,
                              final int typeOfView ,
                              String url ,
                              final String sectionTitle,
                              final OnMediaItemClick itemClick,
                              final int backgroundType,
                              final int typeOfsection,
                              final int mediaType) {

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    MediaResponseModel mediaResponseModel = gson.fromJson(jsonString, MediaResponseModel.class);
                    mediaResponseModel.setSectionTitle(sectionTitle);
                    mediaResponseModel.setSectionType(typeOfsection);
                    if (typeOfView == Constants.TypeOfView.BIG_VIEW){
                        mediaResponseModel.setSectionView(Constants.TypeOfView.BIG_VIEW);
                    }else if (typeOfView == Constants.TypeOfView.NORMAL_VIEW_SPAN_TWO){
                        mediaResponseModel.setSectionView(Constants.TypeOfView.NORMAL_VIEW_SPAN_TWO);
                    } else{
                        mediaResponseModel.setSectionView(Constants.TypeOfView.NORMAL_VIEW);
                    }
                    if (backgroundType == Constants.BackgroundType.BLACK){
                        mediaResponseModel.setBackgroundType(Constants.BackgroundType.BLACK);
                    }else {
                        mediaResponseModel.setBackgroundType(Constants.BackgroundType.WHITE);
                    }
                    datalist.add(mediaResponseModel);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                            MovieSectionAdapter adapter = new MovieSectionAdapter(activity,datalist , itemClick , typeOfsection , mediaType);
                            recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar.setVisibility(View.GONE);
                                    linearLayout.setVisibility(View.VISIBLE);
                                }
                            },500);

                        }
                    });
                }
            }
        });
    }

    public void loadFeaturedVideos(final ArrayList<MovieResults> datalist ,
                                   final RecyclerView sliderView ,
                                   final Activity activity,
                                   String url,
                                   final OnSliderClick listener) {

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    MediaResponseModel mediaResponseModel = gson.fromJson(jsonString, MediaResponseModel.class);
                    ArrayList<MovieResults> helperList = mediaResponseModel.getResults();
                    datalist.addAll(helperList);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            sliderView.setLayoutManager(new LinearLayoutManager(activity , LinearLayoutManager.HORIZONTAL , false));
                            SliderAdapter adapter  = new SliderAdapter(activity , datalist , listener);
                            sliderView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                        }
                    });
                }
            }
        });
    }


    public void detail(final ArrayList<Movie> favMovieList,
                       final ArrayList<Tv_details> tvdatalist,
                       final RecyclerView genrerecyclerView ,
                       final ArrayList<Genre> genredatalist,
                       final int typeOfPicture,
                       final Activity activity  ,
                       String url ,
                       final ImageView backdrop,
                       final ImageView poster,
                       final TextView title,
                       final TextView year,
                       final TextView avarege_vote,
                       final TextView length,
                       final TextView overview,
                       final TextView fav_poster_path) {

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    final Movie movie = gson.fromJson(jsonString, Movie.class);
                    final Tv_details tv_details = gson.fromJson(jsonString, Tv_details.class);
                    tvdatalist.add(tv_details);
                    favMovieList.add(movie);
                    ArrayList<Genre> helperList = movie.getGenres();
                    genredatalist.addAll(helperList);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (!movie.isAdult()) {
                                String backdropUrl = Constants.ImgUrls.POSTER_PATH + movie.getBackdrop_path();
                                Glide
                                        .with(activity)
                                        .load(backdropUrl)
                                        .centerCrop()
                                        .into(backdrop);

                                String posterUrl = Constants.ImgUrls.POSTER_PATH + movie.getPoster_path();
                                Glide
                                        .with(activity)
                                        .load(posterUrl)
                                        .placeholder(R.drawable.ic_movie_placeholder)
                                        .error(R.drawable.ic_movie_placeholder)
                                        .centerCrop()
                                        .into(poster);
                                String movie_TV_history_name;
                                if (typeOfPicture == Constants.TypeOfPicture.MOVIE) {
                                    title.setText(movie.getTitle());
                                    if (movie.getRelease_date() != null && movie.getRelease_date().length() > 3) {
                                        year.setText(movie.getRelease_date().substring(0, 4));
                                    }
                                    length.setText(movie.getRuntime() + "m");
                                    movie_TV_history_name = movie.getTitle();
                                } else {
                                    title.setText(tv_details.getName());
                                    String firstAirDate = tv_details.getFirst_air_date().substring(0, 4);
                                    String lastAirDate = tv_details.getLast_air_date().substring(0, 4);
                                    if (!firstAirDate.equals(lastAirDate)) {
                                        year.setText(firstAirDate + " - " + lastAirDate);
                                    } else {
                                        year.setText(firstAirDate);
                                    }
                                    length.setText(activity.getResources().getString(R.string.tv_show));
                                    movie_TV_history_name = tv_details.getName();

                                }
                                avarege_vote.setText(movie.getVote_average() + "");
                                overview.setText(movie.getOverview());
                                fav_poster_path.setText(movie.getPoster_path());
                                genrerecyclerView.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
                                GenreAdapter adapter = new GenreAdapter(activity, genredatalist);
                                genrerecyclerView.setAdapter(adapter);

                                History.deleteAll(History.class ,"movieid = ? and mediatype = ?" , movie.getId()+"" , typeOfPicture+"");
                                History history = new History(movie.getId(), movie_TV_history_name, movie.getPoster_path(), typeOfPicture);
                                history.save();
                            }else {
                                title.setText(movie.getTitle());
                                overview.setText("No data.");
                                avarege_vote.setText(movie.getVote_average() + "");
                            }
                        }
                    });
                }
            }
        });
    }

    public void cast(final RelativeLayout linearLayout,
                     final ProgressBar progressBar,
                     final RecyclerView recyclerView ,
                     final ArrayList<Cast> datalist ,
                     final Activity activity,
                     String url,
                     final OnCastClick listener){

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    final MovieCreditsResponseModel movieCreditsResponseModel = gson.fromJson(jsonString, MovieCreditsResponseModel.class);
                    ArrayList<Cast> helperList = movieCreditsResponseModel.getCast();
                    datalist.addAll(helperList);

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            recyclerView.setLayoutManager(new LinearLayoutManager(activity , LinearLayoutManager.HORIZONTAL , false));
                            CastAdapter adapter = new CastAdapter(activity , datalist , listener);
                            recyclerView.setAdapter(adapter);

                            progressBar.setVisibility(View.GONE);
                            linearLayout.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }
        });

    }

    public void crew(final RecyclerView recyclerView ,
                     final ArrayList<Crew> datalist ,
                     final Activity activity,
                     String url,
                     final OnCastClick listener,
                     final TextView crew_title){

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    final MovieCreditsResponseModel movieCreditsResponseModel = gson.fromJson(jsonString, MovieCreditsResponseModel.class);
                    ArrayList<Crew> crew = movieCreditsResponseModel.getCrew();
                    for (int i = 0;i<crew.size();i++){
                        Crew crew1 = crew.get(i);
                        if (crew1.getJob().equals("Director") || crew1.getJob().equals("Director") ){
                            datalist.add(crew1);
                        }
                    }
                    for (int i = 0;i<crew.size();i++){
                        Crew crew2 = crew.get(i);
                        if (crew2.getJob().equals("Executive Producer")  ||crew2.getJob().equals("Producer") || crew2.getDepartment().equals("Writing") ||crew2.getDepartment().equals("Sound")){
                            datalist.add(crew2);
                        }
                    }
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (datalist.size() == 0){
                                crew_title.setVisibility(View.GONE);
                            }else {
                                recyclerView.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
                                CrewAdapter adapter = new CrewAdapter(activity, datalist, listener);
                                recyclerView.setAdapter(adapter);
                            }

                        }
                    });
                }
            }
        });

    }

    public void reviews(final RecyclerView recyclerView ,
                        final ArrayList<ReviewResults> datalist ,
                        final Activity activity,
                        String url,
                        final TextView title){

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    final ReviewResponseModel reviewResponseModel = gson.fromJson(jsonString, ReviewResponseModel.class);
                    ArrayList<ReviewResults> helperList = reviewResponseModel.getResults();
                    datalist.addAll(helperList);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (datalist.size() == 0){
                                title.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.GONE);
                            }else {
                                recyclerView.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
                                ReviewsAdapter adapter = new ReviewsAdapter(activity, datalist);
                                recyclerView.setAdapter(adapter);
                            }
                        }
                    });
                }
            }
        });

    }

    public void people(final LinearLayout linearLayout,
                       final ProgressBar progressBar,
                       final ArrayList<PeopleResult> datalist ,
                       final PeopleAdapter adapter,
                       final Activity activity,
                       String url){

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    final PeopleResponseModel peopleResponseModel = gson.fromJson(jsonString, PeopleResponseModel.class);
                    ArrayList<PeopleResult> helperList = peopleResponseModel.getResults();
                    datalist.addAll(helperList);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                            progressBar.setVisibility(View.GONE);
                            linearLayout.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }
        });

    }

    public void person(final LinearLayout linearLayout,
                       final ProgressBar progressBar,
                       final ImageView person_pp,
                       final TextView person_name,
                       final TextView person_biorgaphy,
                       final TextView place_of_birth ,
                       final TextView birthday ,
                       final TextView detaildepartment,
                       final TextView biography_title,
                       final TextView born_title,
                       final Activity activity,
                       String url){

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    final PeopleDetails peopleDetails = gson.fromJson(jsonString, PeopleDetails.class);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            person_name.setText(peopleDetails.getName());
                            person_biorgaphy.setText(peopleDetails.getBiography());
                            if (peopleDetails.getPlace_of_birth() != null){
                                place_of_birth.setText("Place of birth - "+peopleDetails.getPlace_of_birth());
                            }
                            if (peopleDetails.getBirthday() != null){
                                String day = peopleDetails.getBirthday().substring(8,10);
                                String month = peopleDetails.getBirthday().substring(5,7);
                                String year = peopleDetails.getBirthday().substring(0,4) ;
                                birthday.setText("Birthday: "+day+"."+month+"."+year);
                            }
                            if (birthday.getText().equals("") ){
                                born_title.setVisibility(View.GONE);
                            }
                            if (person_biorgaphy.getText().equals("")){
                                biography_title.setVisibility(View.GONE);
                            }
                            if (detaildepartment != null){
                                detaildepartment.setText(peopleDetails.getKnown_for_department());
                            }

                            String ppUrl = Constants.ImgUrls.POSTER_PATH + peopleDetails.getProfile_path();

                            Glide.with(activity)
                                    .asBitmap()
                                    .load(ppUrl)
                                    .placeholder(R.drawable.ic_person_placeholder)
                                    .error(R.drawable.ic_person_placeholder)
                                    .into(new CustomTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                            person_pp.setImageBitmap(resource);
                                        }

                                        @Override
                                        public void onLoadCleared(@Nullable Drawable placeholder) {
                                        }
                                    });

                            History.deleteAll(History.class ,"movieid = ? and mediatype = ?" , peopleDetails.getId()+"" , Constants.TypeOfPicture.PERSON+"");
                            History history = new History(peopleDetails.getId(), peopleDetails.getName(), peopleDetails.getProfile_path(), Constants.TypeOfPicture.PERSON);
                            history.save();

                            progressBar.setVisibility(View.GONE);
                            linearLayout.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }
        });

    }

    public void knownFor(final RecyclerView recyclerView ,
                         final ArrayList<KnownForCast> datalist ,
                         final Activity activity,
                         final TextView actor_popularity,
                         final OnMediaItemClick itemClick,
                         String url){

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    final KnowsForResponseModel knowsForResponseModel = gson.fromJson(jsonString, KnowsForResponseModel.class);
                    ArrayList<KnownForCast> helperList = knowsForResponseModel.getCast();
                    ArrayList<KnownForCast> helperList1 = knowsForResponseModel.getCrew();
                    if (helperList != null && helperList1 != null && helperList.size() > helperList1.size()){
                        datalist.addAll(helperList);
                        datalist.addAll(helperList1);
                    }else if(helperList1 != null && helperList != null && helperList.size() < helperList1.size()){
                        datalist.addAll(helperList1);
                        datalist.addAll(helperList);
                    }
                    float average_vote = 0;
                    int br = 0;
                    for(int i = 0 ; i< datalist.size() ;i++){
                        KnownForCast knownForCast;
                        knownForCast = datalist.get(i);
                        if (knownForCast.getVote_average() > 1) {
                            br++;
                            average_vote += knownForCast.getVote_average();

                        }
                    }
                    average_vote = average_vote / br;
                    DecimalFormat adf = new DecimalFormat();
                    adf.setMaximumFractionDigits(1);
                    final String finalAverage_vote = adf.format(average_vote);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            actor_popularity.setText(finalAverage_vote);
                            recyclerView.setLayoutManager(new LinearLayoutManager(activity , LinearLayoutManager.HORIZONTAL , false));
                            KnownForAdapter adapter = new KnownForAdapter(activity , datalist , itemClick);
                            recyclerView.setAdapter(adapter);
                        }
                    });
                }
            }
        });

    }

    public void search(final ProgressBar progressBar,
                       final RecyclerView recyclerView ,
                       final ArrayList<SearchResults> datalist ,
                       final OnSearchItemClick listener,
                       final Activity activity,
                       String url){

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    final MultiSearchResponseModel multiSearchResponseModel = gson.fromJson(jsonString, MultiSearchResponseModel.class);
                    ArrayList<SearchResults> helperList = multiSearchResponseModel.getResults();
                    datalist.addAll(helperList);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                            SearchAdapter adapter = new SearchAdapter(activity , datalist , listener);
                            recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                            progressBar.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }
        });

    }

    public void pictures(final RecyclerView recyclerView ,
                         final ArrayList<Backdrop> datalist ,
                         final Activity activity,
                         String url,
                         final TextView title,
                         final OnPictureClick listener){

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    final PictureResponseModel pictureResponseModel = gson.fromJson(jsonString, PictureResponseModel.class);
                    ArrayList<Backdrop> helperList = pictureResponseModel.getBackdrops();
                    ArrayList<Backdrop> helperList1 = pictureResponseModel.getPosters();
                    datalist.addAll(helperList);
                    datalist.addAll(helperList1);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (datalist.size() == 0){
                                title.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.GONE);
                            }else {
                                recyclerView.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
                                PictureAdapter adapter = new PictureAdapter(activity, datalist , listener);
                                recyclerView.setAdapter(adapter);
                            }
                        }
                    });
                }
            }
        });

    }

    public void videos(final RecyclerView recyclerView ,
                       final ArrayList<VideoResults> datalist ,
                       final Activity activity,
                       final OnMediaClick listener,
                       final TextView title,
                       final ImageView play_icon,
                       String url){

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    final VideosResponseModel videosResponseModel = gson.fromJson(jsonString, VideosResponseModel.class);
                    ArrayList<VideoResults> helperList = videosResponseModel.getResults();
                    datalist.addAll(helperList);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (datalist.size() == 0){
                                title.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.GONE);
                                play_icon.setVisibility(View.GONE);
                            }else {
                                recyclerView.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
                                VideoAdapter adapter = new VideoAdapter(activity, datalist, listener);
                                recyclerView.setAdapter(adapter);
                            }

                        }
                    });
                }
            }
        });

    }

    public void videoMovieDetail(final int typeOfPicture,
                       final Activity activity  ,
                       String url ,
                       final ImageView poster,
                       final TextView title,
                       final TextView year,
                       final TextView avarege_vote,
                       final TextView length) {

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    final Movie movie = gson.fromJson(jsonString, Movie.class);
                    final Tv_details tv_details = gson.fromJson(jsonString, Tv_details.class);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String posterUrl = Constants.ImgUrls.POSTER_PATH + movie.getPoster_path();
                            Glide
                                    .with(activity)
                                    .load(posterUrl)
                                    .centerCrop()
                                    .into(poster);
                            if(typeOfPicture == Constants.TypeOfPicture.MOVIE){
                                title.setText(movie.getTitle());
                                year.setText(movie.getRelease_date().substring(0,4));
                                length.setText(movie.getRuntime()+"m");
                            }else {
                                title.setText(tv_details.getName());
                                String firstAirDate = tv_details.getFirst_air_date().substring(0,4);
                                String lastAirDate = tv_details.getLast_air_date().substring(0,4) ;
                                if (!firstAirDate.equals(lastAirDate)) {
                                    year.setText(firstAirDate + " - " + lastAirDate);
                                }else {
                                    year.setText(firstAirDate);
                                }
                                length.setText(activity.getResources().getString(R.string.tv_show));

                            }

                            avarege_vote.setText(movie.getVote_average()+"");

                        }
                    });
            }
        }
        });
    }

    public void videosVertical(final RecyclerView recyclerView ,
                               final ArrayList<VideoResults> datalist ,
                               final Activity activity,
                               final OnMediaClick listener,
                               final Transfer transfer,
                               final int position,
                               String url){

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    final VideosResponseModel videosResponseModel = gson.fromJson(jsonString, VideosResponseModel.class);
                    ArrayList<VideoResults> helperList = videosResponseModel.getResults();
                    datalist.addAll(helperList);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                            VideoListAdapter adapter = new VideoListAdapter(activity, datalist ,listener , position , transfer);
                            recyclerView.setAdapter(adapter);

                        }
                    });
                }
            }
        });

    }

    public void picturesAll(final RecyclerView recyclerView ,
                            final List<Backdrop> datalist ,
                            final Activity activity,
                            String url,
                            final OnPictureClick listener,
                            final int position,
                            final TransferPicture transfer,
                            final int type){

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    if (type == Constants.TypeOfPicture.MOVIE || type == Constants.TypeOfPicture.TV ){
                        final PictureResponseModel pictureResponseModel = gson.fromJson(jsonString, PictureResponseModel.class);
                        ArrayList<Backdrop> helperList = pictureResponseModel.getBackdrops();
                        ArrayList<Backdrop> helperList1 = pictureResponseModel.getPosters();
                        datalist.addAll(helperList);
                        datalist.addAll(helperList1);
                    }else{
                        final PeopleImagesResponseModel pictureResponseModel = gson.fromJson(jsonString, PeopleImagesResponseModel.class);
                        ArrayList<Backdrop> helperList = pictureResponseModel.getProfiles();
                        datalist.addAll(helperList);
                    }
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            recyclerView.setLayoutManager(new GridLayoutManager(activity, 3, LinearLayoutManager.VERTICAL, false));
                            PictureListAdapter adapter = new PictureListAdapter(activity, datalist , listener , position , transfer);
                            recyclerView.setAdapter(adapter);

                        }
                    });
                }
            }
        });

    }

    public void seasons(final ArrayList<Seasons> seasonsArrayList,
                       final RecyclerView recyclerView ,
                       final Activity activity ,
                       String url) {

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    final Tv_details tv_details = gson.fromJson(jsonString, Tv_details.class);
                    ArrayList<Seasons> helperList1 = tv_details.getSeasons();
                    seasonsArrayList.addAll(helperList1);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            recyclerView.setLayoutManager(new LinearLayoutManager(activity));
                            SeasonsAdapter adapter = new SeasonsAdapter(activity , seasonsArrayList);
                            recyclerView.setAdapter(adapter);
                        }
                    });
                }
            }
        });
    }

    public void picturesPeople(final RecyclerView recyclerView ,
                         final ArrayList<Backdrop> datalist ,
                         final Activity activity,
                         String url,
                         final TextView title,
                         final OnPictureClick listener){

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    final PeopleImagesResponseModel pictureResponseModel = gson.fromJson(jsonString, PeopleImagesResponseModel.class);
                    ArrayList<Backdrop> helperList = pictureResponseModel.getProfiles();
                    datalist.addAll(helperList);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (datalist.size() == 0){
                                title.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.GONE);
                            }else {
                                if (datalist.size() > 2){
                                    recyclerView.setLayoutManager(new GridLayoutManager(activity, 2, LinearLayoutManager.HORIZONTAL, false));
                                }else {
                                    recyclerView.setLayoutManager(new LinearLayoutManager(activity,  LinearLayoutManager.VERTICAL, false));
                                }
                                PictureAdapter adapter = new PictureAdapter(activity, datalist , listener);
                                recyclerView.setAdapter(adapter);
                            }
                        }
                    });
                }
            }
        });

    }

    public void seeAll(final ArrayList<MovieResults> datalist ,
                       final SeeAllAdapter adapter,
                       final Activity activity,
                       String url) {

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    MediaResponseModel mediaResponseModel = gson.fromJson(jsonString, MediaResponseModel.class);
                    ArrayList<MovieResults> helperList = mediaResponseModel.getResults();
                    datalist.addAll(helperList);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });
    }

    public void front_genres(final RecyclerView recyclerView ,
                             final ArrayList<Genre> datalist ,
                             final Activity activity,
                             final OnGenreClick listener,
                             String url){

        OkHttpClient client = new OkHttpClient();
        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    GenreResponseModel genreResponseModel = gson.fromJson(jsonString, GenreResponseModel.class);
                    ArrayList<Genre> helperList = genreResponseModel.getGenres();
                    datalist.addAll(helperList);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Collections.shuffle(datalist);
                            recyclerView.setLayoutManager(new GridLayoutManager(activity, 2, LinearLayoutManager.HORIZONTAL, false));
                            GenreFrontAdapter adapter = new GenreFrontAdapter(activity , datalist , listener);
                            recyclerView.setAdapter(adapter);
                        }
                    });
                }
            }
        });

    }

    public void loadOscarWinners(final ArrayList<MovieResults> datalist ,
                                 final RecyclerView recyclerView ,
                                 final Activity activity,
                                 String url, final OnMediaItemClick listener) {

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    OscarResponseModel oscarResponseModel = gson.fromJson(jsonString, OscarResponseModel.class);
                    ArrayList<MovieResults> helperList = oscarResponseModel.getItems();
                    datalist.addAll(helperList);
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            recyclerView.setLayoutManager(new LinearLayoutManager(activity ,LinearLayoutManager.HORIZONTAL , false));
                            MovieItemAdapter adapter  = new MovieItemAdapter(activity , datalist , listener , Constants.TypeOfSection.TOP_RATED );
                            recyclerView.setAdapter(adapter);

                        }
                    });
                }
            }
        });
    }

    public void notify(final Activity activity,
                       String url,
                       final TransferNotification notification) {

        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    MediaResponseModel mediaResponseModel = gson.fromJson(jsonString, MediaResponseModel.class);
                    final ArrayList<MovieResults> datalist = mediaResponseModel.getResults();
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String notification_text = "";
                            for (int i = 0 ;i < 7;i++){
                                MovieResults movieResults = datalist.get(i);
                                notification_text+= movieResults.getTitle()+",";

                            }
                            notification.transferNotification(notification_text);

                        }
                    });
                }
            }
        });
    }

}
