package mk.codeacademy.movieapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mk.codeacademy.movieapp.helper.QueryBuilder;
import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.helper.Requests;
import mk.codeacademy.movieapp.adapter.RecentSearchAdapter;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnSearchItemClick;
import mk.codeacademy.movieapp.model.db_models.RecentSearch;
import mk.codeacademy.movieapp.model.search.SearchResults;

public class SearchActivity extends AppCompatActivity implements OnSearchItemClick {

    ProgressBar progressBar;
    RecyclerView searchRecyclerView;
    ArrayList<SearchResults> searchDatalist = new ArrayList<>();
    RecyclerView recent_search_rv;
    List<RecentSearch> recentSearchList = new ArrayList<>();
    RecentSearchAdapter recentSearchAdapter;
    EditText searchEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = findViewById(R.id.search_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressBar = findViewById(R.id.search_pb);
        recent_search_rv = findViewById(R.id.recent_search_rv);
        recent_search_rv.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false));
        loadRecentSearch();

        searchDatalist = new ArrayList<>();
        searchRecyclerView = findViewById(R.id.search_results_recyclerview);
        searchEdit = findViewById(R.id.search_edittext);

        final Requests requests = new Requests();
        final QueryBuilder queryBuilder = new QueryBuilder();

        searchEdit.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        searchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() > 0){
                    progressBar.setVisibility(View.VISIBLE);
                    searchRecyclerView.setVisibility(View.GONE);
                    searchDatalist.clear();
                    String url = queryBuilder.multiSearch(s.toString());
                    requests.search(progressBar ,searchRecyclerView , searchDatalist ,SearchActivity.this ,  SearchActivity.this , url);
                }
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadRecentSearch();
    }

    public void loadRecentSearch(){
        recentSearchList = new ArrayList<>();
        recentSearchList = RecentSearch.listAll(RecentSearch.class);
        Collections.reverse(recentSearchList);
        recentSearchAdapter = new RecentSearchAdapter(this , recentSearchList , this);
        recent_search_rv.setAdapter(recentSearchAdapter);
    }

    @Override
    public void onItemClick(SearchResults results) {
        Intent intent = new Intent();
        String title_name = null;
        switch (results.getMedia_type()){
            case Constants.Media_Type.MOVIE:
                intent = new Intent(this, DetailActivity.class);
                intent.putExtra(Constants.ExtrasKey.ID , results.getId());
                intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , results.getFirst_air_date());
                title_name = results.getTitle();
                break;
            case Constants.Media_Type.TV:
                intent = new Intent(this,DetailActivity.class);
                intent.putExtra(Constants.ExtrasKey.ID , results.getId());
                intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , results.getFirst_air_date());
                title_name = results.getName();
                break;
            case Constants.Media_Type.PERSON:
                intent = new Intent(this, ActorDetailActivity.class);
                intent.putExtra(Constants.ExtrasKey.ID , results.getId());
                title_name = results.getName();
                break;
        }
        RecentSearch.deleteAll(RecentSearch.class ,"movieid = ? and title = ?" , title_name , results.getTitle());
        RecentSearch recentSearch = new RecentSearch(results.getId() , title_name , results.getMedia_type());
        recentSearch.save();

        startActivity(intent);
    }

    @Override
    public void onRecentSearchClick(RecentSearch recentSearch) {
        searchEdit.setText(recentSearch.getTitle());
        searchEdit.setSelection(recentSearch.getTitle().length());
    }
}
