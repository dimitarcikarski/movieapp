package mk.codeacademy.movieapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import java.util.ArrayList;

import mk.codeacademy.movieapp.helper.QueryBuilder;
import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.helper.Requests;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnMediaClick;
import mk.codeacademy.movieapp.interfaces.Transfer;
import mk.codeacademy.movieapp.model.videos.VideoResults;

public class VideoActivity extends AppCompatActivity implements OnMediaClick , Transfer {

    RecyclerView videos_vertical_recyclerView;
    ArrayList<VideoResults> videos_detail_datalist = new ArrayList<>();

    int typeOfPicture;
    int id;
    int position;
    ImageView poster;
    TextView title;
    TextView year;
    TextView avarege_vote;
    TextView length;
    String videoId="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        Toolbar toolbar = findViewById(R.id.video_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.bringToFront();

        if (getIntent().getStringExtra(Constants.ExtrasKey.CONTENT_CHECK) == null){
            typeOfPicture = Constants.TypeOfPicture.MOVIE;
        }else {
            typeOfPicture = Constants.TypeOfPicture.TV;
        }
        id = getIntent().getIntExtra(Constants.ExtrasKey.ID , -1);
        position = getIntent().getIntExtra(Constants.ExtrasKey.VIDEO_POSITION, -1);
        title = findViewById(R.id.video_detail_title);
        year = findViewById(R.id.video_detail_year);
        avarege_vote = findViewById(R.id.video_detail_vote);
        length = findViewById(R.id.video_detail_length_tv_series);
        poster = findViewById(R.id.video_list_poster);
        videos_vertical_recyclerView = findViewById(R.id.videos_vertical_recyclerView);

        Requests requests = new Requests();
        QueryBuilder queryBuilder = new QueryBuilder();
        String detailUrl = queryBuilder.details(id , typeOfPicture);
        String videoUrl = queryBuilder.videos(id , typeOfPicture);

        requests.videoMovieDetail(typeOfPicture , this , detailUrl , poster , title , year , avarege_vote , length);
        requests.videosVertical(videos_vertical_recyclerView,videos_detail_datalist , this ,this ,this,position , videoUrl);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onMovieClick(View view) {

        String type;
        if (typeOfPicture == Constants.TypeOfPicture.MOVIE){
            type = null;
        }else {
            type = Constants.TypeOfPicture.TV_STRING;
        }
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(Constants.ExtrasKey.ID , id);
        intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , type);
        startActivity(intent);

    }

    @Override
    public void onVideoClick(int position) {
        String type;
        if (typeOfPicture == Constants.TypeOfPicture.MOVIE){
            type = null;
        }else {
            type = Constants.TypeOfPicture.TV_STRING;
        }
        Intent intent = new Intent(this, VideoActivity.class);
        intent.putExtra(Constants.ExtrasKey.ID , id);
        intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , type);
        intent.putExtra(Constants.ExtrasKey.VIDEO_POSITION , position);
        startActivity(intent);
        finish();
    }

    @Override
    public void key(String key) {
        videoId = key;
        YouTubePlayerView youTubePlayerView = findViewById(R.id.youtube_player_view);
        getLifecycle().addObserver(youTubePlayerView);
        youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
            @Override
            public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                youTubePlayer.loadVideo(videoId, 0);
                youTubePlayer.play();
            }
        });
    }
}
