package mk.codeacademy.movieapp.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import mk.codeacademy.movieapp.helper.QueryBuilder;
import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.helper.Requests;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnCastClick;
import mk.codeacademy.movieapp.interfaces.OnMediaClick;
import mk.codeacademy.movieapp.interfaces.OnMediaItemClick;
import mk.codeacademy.movieapp.interfaces.OnPictureClick;
import mk.codeacademy.movieapp.model.db_models.FavMovie;
import mk.codeacademy.movieapp.model.db_models.FavTvShows;
import mk.codeacademy.movieapp.model.db_models.Watchlist;
import mk.codeacademy.movieapp.model.discover_movie.MediaResponseModel;
import mk.codeacademy.movieapp.model.discover_movie.MovieResults;
import mk.codeacademy.movieapp.model.movie_credits.Cast;
import mk.codeacademy.movieapp.model.movie_credits.Crew;
import mk.codeacademy.movieapp.model.movie_credits.KnownForCast;
import mk.codeacademy.movieapp.model.movie_details.Genre;
import mk.codeacademy.movieapp.model.movie_details.Movie;
import mk.codeacademy.movieapp.model.pictures.Backdrop;
import mk.codeacademy.movieapp.model.reviews.ReviewResults;
import mk.codeacademy.movieapp.model.tv_details.Tv_details;
import mk.codeacademy.movieapp.model.videos.VideoResults;

public class DetailActivity extends AppCompatActivity implements OnMediaItemClick , OnCastClick , OnMediaClick , OnPictureClick {

    RelativeLayout linear_layout_detail;
    LinearLayout detail_container;
    ProgressBar progressBar;
    ArrayList<Movie> favMovieList = new ArrayList<>();
    RecyclerView similarRecyclerView;
    ArrayList<MediaResponseModel> similarDatalist = new ArrayList<>();
    RecyclerView castRecyclerView;
    ArrayList<Cast> castDatalist = new ArrayList<>();
    RecyclerView crewRecyclerView;
    ArrayList<Crew> crewDatalist = new ArrayList<>();
    RecyclerView reviewsRecyclerView;
    ArrayList<ReviewResults> reviewsDatalist = new ArrayList<>();
    ArrayList<Tv_details> tVdetailDataList = new ArrayList<>();
    RecyclerView pictures_detail_recyclerView;
    ArrayList<Backdrop> pictures_detail = new ArrayList<>();
    RecyclerView videos_detail_recyclerView;
    ArrayList<VideoResults> videos_detail_datalist = new ArrayList<>();
    RecyclerView genreRecyclerView;
    ArrayList<Genre> genreDatalist = new ArrayList<>();

    ImageView backdrop;
    ImageView poster;
    ImageView play_icon;
    TextView crew_title;
    TextView title;
    TextView year;
    TextView avarege_vote;
    TextView length;
    TextView overview;
    TextView fav_poster_path;
    TextView videos_title;
    TextView images_title;
    TextView reviews_title;
    Button seasons_button;
    int id;
    String favtitle;
    String favposter;
    int typeOfPicture;
    int maxLines = 0;
    int favoured = 0;
    int watchlisted = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = findViewById(R.id.toolbar_detail);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        findViewById(R.id.detail_appbar).bringToFront();

        linear_layout_detail = findViewById(R.id.linear_layout_detail);
        progressBar = findViewById(R.id.detail_pb);
        detail_container = findViewById(R.id.detail_container);

        backdrop = findViewById(R.id.backdrop_poster_detail);
        poster = findViewById(R.id.detail_poster);
        title = findViewById(R.id.detail_title);
        year = findViewById(R.id.detail_year);
        avarege_vote = findViewById(R.id.detail_vote);
        length = findViewById(R.id.detail_length_tv_series);
        overview = findViewById(R.id.detail_overview);
        fav_poster_path = findViewById(R.id.fav_poster_path);
        play_icon = findViewById(R.id.play_icon);
        crew_title = findViewById(R.id.crew_title);

        similarRecyclerView  = findViewById(R.id.similar_recyclerView);
        castRecyclerView = findViewById(R.id.cast_recyclerView);
        crewRecyclerView = findViewById(R.id.crew_recyclerView);
        reviewsRecyclerView  = findViewById(R.id.reviews_recyclerView);
        pictures_detail_recyclerView = findViewById(R.id.pictures_detail_recyclerView);
        videos_detail_recyclerView = findViewById(R.id.videos_detail_recyclerView);
        videos_title = findViewById(R.id.detail_videos_title);
        images_title= findViewById(R.id.detail_images_title);
        reviews_title= findViewById(R.id.reviews_title);
        genreRecyclerView = findViewById(R.id.genre_recyclerView);
        seasons_button = findViewById(R.id.seasons_button);

        overview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (maxLines == 0) {
                    overview.setMaxLines(100);
                    maxLines = 1;
                }else if (maxLines == 1){
                    overview.setMaxLines(4);
                    maxLines = 0;
                }
            }
        });


        if (getIntent().getStringExtra(Constants.ExtrasKey.CONTENT_CHECK) == null){
            typeOfPicture = Constants.TypeOfPicture.MOVIE;
        }else {
            typeOfPicture = Constants.TypeOfPicture.TV;
        }
        id = getIntent().getIntExtra(Constants.ExtrasKey.ID , -1);

        Requests requests = new Requests();
        QueryBuilder queryBuilder = new QueryBuilder();
        similarDatalist = new ArrayList<>();
        castDatalist = new ArrayList<>();
        reviewsDatalist = new ArrayList<>();

        if (typeOfPicture == Constants.TypeOfPicture.MOVIE){
            seasons_button.setVisibility(View.GONE);
        }

        String detailUrl = queryBuilder.details(id,typeOfPicture);
        String castUrl = queryBuilder.cast(id,typeOfPicture);
        String similarUrl = queryBuilder.similar(id,typeOfPicture);
        String reviewUrl = queryBuilder.review(id,typeOfPicture);
        String picturesUrl = queryBuilder.pictures(id , typeOfPicture);
        String videoUrl = queryBuilder.videos(id , typeOfPicture);

        requests.detail(favMovieList , tVdetailDataList,genreRecyclerView ,
                genreDatalist,typeOfPicture, this , detailUrl ,
                backdrop , poster , title , year , avarege_vote ,
                length , overview ,fav_poster_path);
        requests.loadMoviesTvs(detail_container , progressBar ,similarRecyclerView ,similarDatalist , this ,Constants.TypeOfView.NORMAL_VIEW, similarUrl, "Similar", this, Constants.BackgroundType.WHITE ,Constants.TypeOfSection.SIMILAR,typeOfPicture);
        requests.cast(linear_layout_detail , progressBar ,castRecyclerView , castDatalist , this , castUrl , this);
        requests.crew(crewRecyclerView , crewDatalist , this , castUrl , this , crew_title);
        requests.reviews(reviewsRecyclerView,reviewsDatalist , this , reviewUrl , reviews_title);
        requests.pictures(pictures_detail_recyclerView,pictures_detail , this , picturesUrl , images_title , this);
        requests.videos(videos_detail_recyclerView,videos_detail_datalist , this ,this, videos_title , play_icon, videoUrl );


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_actionbar_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        checkFavoured();
        if (favoured == 1) {
            menu.getItem(0).setIcon(R.drawable.baseline_favorite_white_48);
        }
        checkWatchlist();
        if (watchlisted == 1){
            menu.getItem(1).setIcon(R.drawable.ic_added_to_watchlist);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        favtitle = title.getText().toString();
        favposter = fav_poster_path.getText().toString();
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            case R.id.addToFavourites:
                addOrRemoveFromFav(item);
                return true;
            case R.id.addToWatchlist:
                addOrRemoveFromWatchList(item);
                return true;
            case R.id.sharePicture:
                return true;
        }
        return false;
    }

    void checkFavoured(){
        if (typeOfPicture == Constants.TypeOfPicture.MOVIE){
            List<FavMovie> list = FavMovie.find(FavMovie.class ,"movieid = ?" , id+"");
            if (list.size() > 0){
                favoured = 1;

            }
        }else {
            List<FavTvShows> list = FavTvShows.find(FavTvShows.class , "movieid = ?" , id+"");
            if (list.size() > 0){
                favoured = 1;
            }
        }
    }

    void checkWatchlist(){
        List<Watchlist> list = Watchlist.find(Watchlist.class ,"movieid = ?" , id+"");
        if (list.size() > 0){
            watchlisted = 1;
        }
    }

    @Override
    public void onItemClick(MovieResults movie) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(Constants.ExtrasKey.ID , movie.getId());
        intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , movie.getFirst_air_date());
        startActivity(intent);
    }

    @Override
    public void onKnownForClick(KnownForCast knownForCast) {

    }

    @Override
    public void onSeeAllClick(MediaResponseModel mediaResponseModel, int mediaType) {

    }

    @Override
    public void onCastClick(Cast cast) {
        Intent intent = new Intent(this, ActorDetailActivity.class);
        intent.putExtra(Constants.ExtrasKey.ID , cast.getId());
        startActivity(intent);
    }

    @Override
    public void onCrewClick(Crew crew) {
        Intent intent = new Intent(this, ActorDetailActivity.class);
        intent.putExtra(Constants.ExtrasKey.ID , crew.getId());
        startActivity(intent);
    }



    public void onPlayClick(View view) {
        if (play_icon.getVisibility() != View.GONE){
            String type;
            if (typeOfPicture == Constants.TypeOfPicture.MOVIE){
                type = null;
            }else {
                type = Constants.TypeOfPicture.TV_STRING;
            }
            Intent intent = new Intent(this, VideoActivity.class);
            intent.putExtra(Constants.ExtrasKey.ID , id);
            intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , type);
            intent.putExtra(Constants.ExtrasKey.VIDEO_POSITION , 0);
            startActivity(intent);
        }
    }

    @Override
    public void onVideoClick(int position) {
        String type;
        if (typeOfPicture == Constants.TypeOfPicture.MOVIE){
            type = null;
        }else {
            type = Constants.TypeOfPicture.TV_STRING;
        }
        Intent intent = new Intent(this, VideoActivity.class);
        intent.putExtra(Constants.ExtrasKey.ID , id);
        intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , type);
        intent.putExtra(Constants.ExtrasKey.VIDEO_POSITION , position);
        startActivity(intent);
    }

    @Override
    public void onPictureClick(int position) {
        String type;
        if (typeOfPicture == Constants.TypeOfPicture.MOVIE){
            type = null;
        }else {
            type = Constants.TypeOfPicture.TV_STRING;
        }
        Intent intent = new Intent(this, ImagesActivity.class);
        intent.putExtra(Constants.ExtrasKey.ID , id);
        intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , type);
        intent.putExtra(Constants.ExtrasKey.VIDEO_POSITION , position);
        startActivity(intent);
    }

    public void seeSeasons(View view) {
        Intent intent = new Intent(this, SeasonsActivity.class);
        intent.putExtra(Constants.ExtrasKey.ID , id);
        startActivity(intent);
    }

    public void addOrRemoveFromFav(MenuItem item){
        if (favoured == 0) {
            if (typeOfPicture == Constants.TypeOfPicture.MOVIE) {
                FavMovie favMovie = new FavMovie(id, favtitle, favposter , typeOfPicture);
                favMovie.save();
                favoured = 1;
                item.setIcon(R.drawable.baseline_favorite_white_48);
                Toast.makeText(this, "Added to Favourite Movies", Toast.LENGTH_SHORT).show();

            } else {
                FavTvShows favTvShows = new FavTvShows(id, favtitle, favposter , typeOfPicture);
                favTvShows.save();
                favoured = 1;
                item.setIcon(R.drawable.baseline_favorite_white_48);
                Toast.makeText(this, "Added to Favourite TV-Shows", Toast.LENGTH_SHORT).show();
            }
        }else{
            if (typeOfPicture == Constants.TypeOfPicture.MOVIE){
                FavMovie.deleteAll(FavMovie.class ,"movieid = ?" , id+"");
                favoured = 0;
                item.setIcon(R.drawable.baseline_favorite_border_white_48);
                Toast.makeText(this, "Removed from Favourite Movies", Toast.LENGTH_SHORT).show();

            }else {
                FavTvShows.deleteAll(FavTvShows.class ,"movieid = ?" , id+"" );
                favoured = 0;
                item.setIcon(R.drawable.baseline_favorite_border_white_48);
                Toast.makeText(this, "Removed from Favourite TV-Shows", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void addOrRemoveFromWatchList(MenuItem item){
        if (watchlisted == 0) {
            Watchlist watchlist = new Watchlist(id, favtitle, favposter , typeOfPicture);
            watchlist.save();
            watchlisted = 1;
            item.setIcon(R.drawable.ic_added_to_watchlist);
            Toast.makeText(this, "Added to Watchlist", Toast.LENGTH_SHORT).show();

        }else{
            Watchlist.deleteAll(Watchlist.class ,"movieid = ?" , id+"");
            watchlisted = 0;
            item.setIcon(R.drawable.ic_add_to_watchlist);
            Toast.makeText(this, "Removed from Watchlist", Toast.LENGTH_SHORT).show();

        }
    }
}
