package mk.codeacademy.movieapp.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.MenuItem;

import java.util.ArrayList;

import mk.codeacademy.movieapp.helper.QueryBuilder;
import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.helper.Requests;
import mk.codeacademy.movieapp.adapter.SeeAllAdapter;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnClickSeeAll;
import mk.codeacademy.movieapp.model.discover_movie.MovieResults;

public class SeeAllActivity extends AppCompatActivity implements OnClickSeeAll {

    int sectionType;
    int mediaType;
    RecyclerView seeAllRecyclerview;
    ArrayList<MovieResults> datalist = new ArrayList<>();
    SeeAllAdapter adapter;
    boolean isloading = false;
    Requests requests = new Requests();
    QueryBuilder queryBuilder = new QueryBuilder();
    int page = 1;
    String url = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_all);
        Toolbar toolbar = findViewById(R.id.seeAll_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        seeAllRecyclerview = findViewById(R.id.seeAll_recyclerView);

        mediaType = getIntent().getIntExtra(Constants.ExtrasKey.MEDIA_TYPE, -1);
        sectionType = getIntent().getIntExtra(Constants.ExtrasKey.SECTION_TYPE, -1);

        seeAllRecyclerview.setLayoutManager(new LinearLayoutManager(this));
        adapter  = new SeeAllAdapter(this , datalist , this , mediaType);
        seeAllRecyclerview.setAdapter(adapter);

        requestPage(mediaType , sectionType , page);

        initScrollListener();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClickSeeAll(MovieResults movieResults) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(Constants.ExtrasKey.ID , movieResults.getId());
        intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , movieResults.getFirst_air_date());
        startActivity(intent);
    }

    private void initScrollListener(){
        seeAllRecyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (!isloading){
                    if(linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == datalist.size() - 1){
                        int nextPage = page + 1;
                        if (page <= 11) {
                            loadMore(nextPage);
                        }
                        isloading = true;
                    }
                }
            }
        });
    }

    public void requestPage(int mediaType , int sectionType , int page){
        if (mediaType == Constants.TypeOfPicture.MOVIE){
            switch (sectionType){
                case Constants.MovieSectionType.POPULAR:
                    getSupportActionBar().setTitle("Popular Movies");
                    url = queryBuilder.movies(Constants.MovieSectionType.POPULAR , page+"");
                    break;
                case Constants.MovieSectionType.UPCOMMING:
                    getSupportActionBar().setTitle("Upcoming Movies");
                    url = queryBuilder.movies(Constants.MovieSectionType.UPCOMMING , page+"");
                    break;
                case Constants.MovieSectionType.TOP_RATED:
                    getSupportActionBar().setTitle("Top Rated Movies");
                    url = queryBuilder.movies(Constants.MovieSectionType.TOP_RATED, page+"");
                    break;

            }
        }else if (mediaType == Constants.TypeOfPicture.TV){
            switch (sectionType){
                case Constants.TvSectionType.POPULAR:
                    getSupportActionBar().setTitle("Popular Tv Shows");
                    url = queryBuilder.tvShows(Constants.TvSectionType.POPULAR, page+"");
                    break;
                case Constants.TvSectionType.AIRING_TODAY:
                    getSupportActionBar().setTitle("Airing Today");
                    url = queryBuilder.tvShows(Constants.TvSectionType.AIRING_TODAY, page+"");
                    break;
                case Constants.TvSectionType.TOP_RATED:
                    getSupportActionBar().setTitle("Top Rated Tv Shows");
                    url = queryBuilder.tvShows(Constants.TvSectionType.TOP_RATED, page+"");
                    break;

            }
        }else if (mediaType == Constants.TypeOfPicture.GENRE){
            int id = getIntent().getIntExtra(Constants.ExtrasKey.GENRE_TYPE_ID , -1);
            getSupportActionBar().setTitle(getIntent().getStringExtra(Constants.ExtrasKey.GENRE_TYPE_NAME));
            url = queryBuilder.genreDetail(id+"" , page+"");
        }

        requests.seeAll(datalist,adapter, this , url);
    }

    private void loadMore(final int nextPage) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                requestPage(mediaType , sectionType , nextPage);
                page++;
                isloading = false;
            }
        } , 100);
    }
}
