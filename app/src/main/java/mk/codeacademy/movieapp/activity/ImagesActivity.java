package mk.codeacademy.movieapp.activity;

import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.stfalcon.imageviewer.StfalconImageViewer;
import com.stfalcon.imageviewer.loader.ImageLoader;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuItem;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import mk.codeacademy.movieapp.helper.QueryBuilder;
import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.helper.Requests;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnPictureClick;
import mk.codeacademy.movieapp.interfaces.TransferPicture;
import mk.codeacademy.movieapp.model.pictures.Backdrop;

public class ImagesActivity extends AppCompatActivity implements OnPictureClick, TransferPicture {

    List<Backdrop> pictures_detail = new ArrayList<>();
    RecyclerView pictures_detail_recyclerView;

    int typeOfPicture;
    int id;
    int firstposition;
    int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images);
        Toolbar toolbar = findViewById(R.id.images_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (getIntent().getStringExtra(Constants.ExtrasKey.CONTENT_CHECK) == null){
            typeOfPicture = Constants.TypeOfPicture.MOVIE;
            type = Constants.TypeOfPicture.MOVIE;
        }else if (getIntent().getStringExtra(Constants.ExtrasKey.CONTENT_CHECK).equals(Constants.Media_Type.PERSON)) {
            typeOfPicture = Constants.TypeOfPicture.PERSON;
            type = Constants.TypeOfPicture.PERSON;
        }else {
            typeOfPicture = Constants.TypeOfPicture.TV;
            type = Constants.TypeOfPicture.TV;
        }
        id = getIntent().getIntExtra(Constants.ExtrasKey.ID , -1);
        firstposition = getIntent().getIntExtra(Constants.ExtrasKey.VIDEO_POSITION, -1);

        pictures_detail_recyclerView = findViewById(R.id.recycler_view_images);

        Requests requests = new Requests();
        QueryBuilder queryBuilder = new QueryBuilder();

        String picturesUrl = queryBuilder.pictures(id , typeOfPicture);
        requests.picturesAll(pictures_detail_recyclerView,pictures_detail , this , picturesUrl , this,firstposition , this, type);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPictureClick(int position) {
        new StfalconImageViewer.Builder<>(this, pictures_detail, new ImageLoader<Backdrop>() {
            @Override
            public void loadImage(ImageView imageView, Backdrop drawableRes) {
                String imgUrl = Constants.ImgUrls.POSTER_PATH + drawableRes.getFile_path();
                Glide
                        .with(ImagesActivity.this)
                        .load(imgUrl)
                        .into(imageView);
                imageView.setBackgroundColor(getResources().getColor(R.color.black));
            }
        }).withStartPosition(position).withHiddenStatusBar(false).show();
    }

    @Override
    public void picturePosition(int position) {
        new StfalconImageViewer.Builder<>(this, pictures_detail, new ImageLoader<Backdrop>() {
            @Override
            public void loadImage(ImageView imageView, Backdrop drawableRes) {
                String imgUrl = Constants.ImgUrls.POSTER_PATH + drawableRes.getFile_path();
                Glide
                        .with(ImagesActivity.this)
                        .load(imgUrl)
                        .into(imageView);
                imageView.setBackgroundColor(getResources().getColor(R.color.black));
            }
        }).withStartPosition(position).withHiddenStatusBar(false).show();
    }
}
