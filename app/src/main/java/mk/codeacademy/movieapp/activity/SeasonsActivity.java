package mk.codeacademy.movieapp.activity;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuItem;

import java.util.ArrayList;

import mk.codeacademy.movieapp.helper.QueryBuilder;
import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.helper.Requests;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.model.tv_details.Seasons;

public class SeasonsActivity extends AppCompatActivity {

    RecyclerView seasonsRecyclerView;
    ArrayList<Seasons> seasonsDatalist = new ArrayList<>();
    int id;
    int typeOfpicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seasons);
        Toolbar toolbar = findViewById(R.id.seasons_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Seasons");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        id = getIntent().getIntExtra(Constants.ExtrasKey.ID , -1);
        typeOfpicture = Constants.TypeOfPicture.TV;
        seasonsRecyclerView = findViewById(R.id.seasons_recycler);

        final Requests requests = new Requests();
        final QueryBuilder queryBuilder = new QueryBuilder();

        String url = queryBuilder.details(id,typeOfpicture);
        requests.seasons(seasonsDatalist,seasonsRecyclerView,this,url);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }



}
