package mk.codeacademy.movieapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

import mk.codeacademy.movieapp.MainActivity;
import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.broadcast_receiver.AlarmBroadcastReceiver;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.helper.QueryBuilder;
import mk.codeacademy.movieapp.helper.Requests;
import mk.codeacademy.movieapp.interfaces.TransferNotification;

public class SplashActivity extends AppCompatActivity implements TransferNotification {

    SharedPreferences sharedPreferences;
    ImageView logo;
    LinearLayout genreLayout;
    int genreCounter = 0;
    int genreOne = 0 ;
    int  genreTwo = 0;
    int genreThree = 0;
    int dramaCheck = 0 , adventureCheck = 0 ,
            comedyCheck = 0 , romanceCheck = 0,
            actionCheck = 0 , scifiCheck = 0 ,
            horrorCheck = 0 , thrillerCheck = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        logCheck();

        sharedPreferences = getSharedPreferences("MY_SHARED_PREF",MODE_PRIVATE);
        logo = findViewById(R.id.logo);
        genreLayout = findViewById(R.id.choose_genre_layout);

        Requests requests = new Requests();
        QueryBuilder queryBuilder = new QueryBuilder();

        String url = queryBuilder.trending();
        requests.notify(this, url, this);
    }

    public void onGenreClick(View view) {
        TextView textView = (TextView) view;
            switch (view.getId()) {
                case R.id.drama:
                    if (dramaCheck == 0) {
                        if (genreOne == 0) {
                            genreOne = Constants.Genres.DRAMA;
                        } else if (genreTwo == 0) {
                            genreTwo = Constants.Genres.DRAMA;
                        } else if (genreThree == 0) {
                            genreTwo = Constants.Genres.DRAMA;
                        }
                        genreCounter++;
                        dramaCheck = 1;
                        textView.setBackground(getResources().getDrawable(R.drawable.round_button_genre_selected));
                        textView.setTextColor(getResources().getColor(R.color.black));
                    }else{
                        genreCounter--;
                        dramaCheck = 0;
                        textView.setBackground(getResources().getDrawable(R.drawable.round_button_genre));
                        textView.setTextColor(getResources().getColor(R.color.main_text_color));
                    }
                    break;
                case R.id.adventure:
                    if (adventureCheck == 0) {
                        if (genreOne == 0) {
                            genreOne = Constants.Genres.ADVENTURE;
                        } else if (genreTwo == 0) {
                            genreTwo = Constants.Genres.ADVENTURE;
                        } else if (genreThree == 0) {
                            genreTwo = Constants.Genres.ADVENTURE;
                        }
                        genreCounter++;
                        adventureCheck = 1;
                        textView.setBackground(getResources().getDrawable(R.drawable.round_button_genre_selected));
                        textView.setTextColor(getResources().getColor(R.color.black));
                    }else {
                        genreCounter--;
                        adventureCheck = 0;
                        textView.setBackground(getResources().getDrawable(R.drawable.round_button_genre));
                        textView.setTextColor(getResources().getColor(R.color.main_text_color));
                    }
                    break;
                case R.id.romance:
                    if (romanceCheck == 0) {
                        if (genreOne == 0) {
                            genreOne = Constants.Genres.ROMANCE;
                        } else if (genreTwo == 0) {
                            genreTwo = Constants.Genres.ROMANCE;
                        } else if (genreThree == 0) {
                            genreTwo = Constants.Genres.ROMANCE;
                        }
                        genreCounter++;
                        romanceCheck = 1;
                        textView.setBackground(getResources().getDrawable(R.drawable.round_button_genre_selected));
                        textView.setTextColor(getResources().getColor(R.color.black));
                    }else {
                        genreCounter--;
                        romanceCheck = 0;
                        textView.setBackground(getResources().getDrawable(R.drawable.round_button_genre));
                        textView.setTextColor(getResources().getColor(R.color.main_text_color));
                    }
                    break;
                case R.id.comedy:
                    if (comedyCheck == 0) {
                        if (genreOne == 0) {
                            genreOne = Constants.Genres.COMEDY;
                        } else if (genreTwo == 0) {
                            genreTwo = Constants.Genres.COMEDY;
                        } else if (genreThree == 0) {
                            genreTwo = Constants.Genres.COMEDY;
                        }
                        genreCounter++;
                        comedyCheck = 1;
                        textView.setBackground(getResources().getDrawable(R.drawable.round_button_genre_selected));
                        textView.setTextColor(getResources().getColor(R.color.black));
                    }else {
                        genreCounter--;
                        comedyCheck = 0;
                        textView.setBackground(getResources().getDrawable(R.drawable.round_button_genre));
                        textView.setTextColor(getResources().getColor(R.color.main_text_color));
                    }
                    break;
                case R.id.action:
                    if (actionCheck == 0) {
                        if (genreOne == 0) {
                            genreOne = Constants.Genres.ACTION;
                        } else if (genreTwo == 0) {
                            genreTwo = Constants.Genres.ACTION;
                        } else if (genreThree == 0) {
                            genreTwo = Constants.Genres.ACTION;
                        }
                        genreCounter++;
                        actionCheck = 1;
                        textView.setBackground(getResources().getDrawable(R.drawable.round_button_genre_selected));
                        textView.setTextColor(getResources().getColor(R.color.black));
                    }else {
                        genreCounter--;
                        actionCheck = 0;
                        textView.setBackground(getResources().getDrawable(R.drawable.round_button_genre));
                        textView.setTextColor(getResources().getColor(R.color.main_text_color));
                    }
                    break;
                case R.id.sci_fi:
                    if (scifiCheck == 0) {
                        if (genreOne == 0) {
                            genreOne = Constants.Genres.SCI_FI;
                        } else if (genreTwo == 0) {
                            genreTwo = Constants.Genres.SCI_FI;
                        } else if (genreThree == 0) {
                            genreTwo = Constants.Genres.SCI_FI;
                        }
                        genreCounter++;
                        scifiCheck = 1;
                        textView.setBackground(getResources().getDrawable(R.drawable.round_button_genre_selected));
                        textView.setTextColor(getResources().getColor(R.color.black));
                    }else {
                        genreCounter--;
                        scifiCheck = 0;
                        textView.setBackground(getResources().getDrawable(R.drawable.round_button_genre));
                        textView.setTextColor(getResources().getColor(R.color.main_text_color));
                    }
                    break;
                case R.id.horror:
                    if (horrorCheck == 0) {
                        if (genreOne == 0) {
                            genreOne = Constants.Genres.HORROR;
                        } else if (genreTwo == 0) {
                            genreTwo = Constants.Genres.HORROR;
                        } else if (genreThree == 0) {
                            genreTwo = Constants.Genres.HORROR;
                        }
                        genreCounter++;
                        horrorCheck = 1;
                        textView.setBackground(getResources().getDrawable(R.drawable.round_button_genre_selected));
                        textView.setTextColor(getResources().getColor(R.color.black));
                    }else {
                        genreCounter--;
                        horrorCheck = 0;
                        textView.setBackground(getResources().getDrawable(R.drawable.round_button_genre));
                        textView.setTextColor(getResources().getColor(R.color.main_text_color));
                    }
                    break;
                case R.id.thriller:
                    if (thrillerCheck == 0) {
                        if (genreOne == 0) {
                            genreOne = Constants.Genres.THRILLER;
                        } else if (genreTwo == 0) {
                            genreTwo = Constants.Genres.THRILLER;
                        } else if (genreThree == 0) {
                            genreTwo = Constants.Genres.THRILLER;
                        }
                        genreCounter++;
                        thrillerCheck = 1;
                        textView.setBackground(getResources().getDrawable(R.drawable.round_button_genre_selected));
                        textView.setTextColor(getResources().getColor(R.color.black));
                    }else {
                        genreCounter--;
                        thrillerCheck = 0;
                        textView.setBackground(getResources().getDrawable(R.drawable.round_button_genre));
                        textView.setTextColor(getResources().getColor(R.color.main_text_color));
                    }
                    break;

            }

    }

    public void onContinue(View view) {
        if (genreCounter==3) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("genre1", genreOne + "");
            editor.putString("genre2", "|" + genreTwo);
            editor.putString("genre3", "|" + genreThree);
            editor.putInt("genreChosen",1);
            editor.apply();

            Intent i = new Intent(SplashActivity.this, BoardingActivity.class);
            startActivity(i);
            finish();
        }else {
            Toast.makeText(this, "Choose only three genres.", Toast.LENGTH_LONG).show();
        }
    }

    public void logCheck(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(sharedPreferences.getInt("genreChosen",-1) == 1){
                    Intent i = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                }else{
                    logo.setVisibility(View.GONE);
                    genreLayout.setVisibility(View.VISIBLE);
                }
            }
        },1000);
    }

    public void registerBroadcast(){
        Intent _intent = new Intent(this, AlarmBroadcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, _intent, 0);
        AlarmManager alarmManager = (AlarmManager)this.getSystemService(this.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
    }

    @Override
    public void transferNotification(String text) {
        if (sharedPreferences.getInt("genreChosen",-1) != 1) {
            registerBroadcast();
        }
    }
}
