package mk.codeacademy.movieapp.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import mk.codeacademy.movieapp.helper.QueryBuilder;
import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.helper.Requests;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnMediaItemClick;
import mk.codeacademy.movieapp.interfaces.OnPictureClick;
import mk.codeacademy.movieapp.model.discover_movie.MediaResponseModel;
import mk.codeacademy.movieapp.model.discover_movie.MovieResults;
import mk.codeacademy.movieapp.model.movie_credits.KnownForCast;
import mk.codeacademy.movieapp.model.movie_credits.KnowsForResponseModel;
import mk.codeacademy.movieapp.model.pictures.Backdrop;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Response;

public class ActorDetailActivity extends AppCompatActivity implements OnMediaItemClick , OnPictureClick {

    LinearLayout linear_layout_actor_detail;
    ProgressBar progressBar;

    ImageView person_pp;
    TextView person_name;
    TextView person_biorgaphy;
    TextView actor_popularity;
    TextView place_of_birth;
    TextView birthday;
    TextView detaildepartment;
    TextView actor_images_title;
    TextView biography_title;
    TextView born_title;

    int id;
    int maxLines = 0;

    RecyclerView knownForRecyclerView;
    ArrayList<KnownForCast> knownDatalist = new ArrayList<>();
    ArrayList<KnownForCast> randomDatalist = new ArrayList<>();
    RecyclerView actor_images;
    ArrayList<Backdrop> actor_images_datalist = new ArrayList<>();
    Gson gson;
    KnownForCast randomResult;
    String knownForUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actor_detail);
        linear_layout_actor_detail = findViewById(R.id.linear_layout_actor_detail);
        progressBar = findViewById(R.id.actorDetail_pb);

        Toolbar toolbar = findViewById(R.id.actor_detail_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        findViewById(R.id.appbar_actordetail).bringToFront();

        Requests requests = new Requests();
        QueryBuilder queryBuilder = new QueryBuilder();
        gson = new Gson();


        id = getIntent().getIntExtra(Constants.ExtrasKey.ID , -1);

        person_biorgaphy = findViewById(R.id.person_biography);
        person_name = findViewById(R.id.person_name);
        person_pp = findViewById(R.id.person_profile_picture);
        knownForRecyclerView = findViewById(R.id.known_for_recyclerView);
        actor_popularity = findViewById(R.id.actor_popularity);
        place_of_birth = findViewById(R.id.place_of_birth);
        birthday = findViewById(R.id.birthday);
        detaildepartment = findViewById(R.id.detaildepartment);
        actor_images_title = findViewById(R.id.actor_images_title);
        actor_images = findViewById(R.id.actor_images);
        biography_title = findViewById(R.id.biography_title);
        born_title = findViewById(R.id.born_title);

        knownForUrl  = queryBuilder.knownForCombined(id);
        String detailUrl = queryBuilder.person(id);
        String peopleImagesUrl = queryBuilder.pictures(id , Constants.TypeOfPicture.PERSON);

        requests.knownFor(knownForRecyclerView , knownDatalist , this , actor_popularity , this , knownForUrl);
        requests.person(linear_layout_actor_detail,progressBar, person_pp , person_name , person_biorgaphy,place_of_birth , birthday , detaildepartment,biography_title,born_title, this , detailUrl);
        requests.picturesPeople(actor_images,actor_images_datalist,this , peopleImagesUrl ,actor_images_title,this );

        person_biorgaphy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (maxLines == 0) {
                    person_biorgaphy.setMaxLines(100);
                    maxLines = 1;
                }else if (maxLines == 1){
                    person_biorgaphy.setMaxLines(4);
                    maxLines = 0;
            }
            }
        });



    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(MovieResults movie) {

    }

    @Override
    public void onKnownForClick(KnownForCast knownForCast) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(Constants.ExtrasKey.ID , knownForCast.getId());
        intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , knownForCast.getFirst_air_date());
        startActivity(intent);
    }

    @Override
    public void onSeeAllClick(MediaResponseModel mediaResponseModel, int mediaType) {

    }

    @Override
    public void onPictureClick(int position) {
        Intent intent = new Intent(this, ImagesActivity.class);
        intent.putExtra(Constants.ExtrasKey.ID , id);
        intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , Constants.Media_Type.PERSON);
        intent.putExtra(Constants.ExtrasKey.VIDEO_POSITION , position);
        startActivity(intent);
    }

    public void onActorDice(View view) {
        OkHttpClient client = new OkHttpClient();

        final okhttp3.Request request = new okhttp3.Request.Builder().url(knownForUrl).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    final KnowsForResponseModel knowsForResponseModel = gson.fromJson(jsonString, KnowsForResponseModel.class);
                    ArrayList<KnownForCast> helperList = knowsForResponseModel.getCast();
                    ArrayList<KnownForCast> helperList1 = knowsForResponseModel.getCrew();
                    if (helperList != null && helperList1 != null && helperList.size() > helperList1.size()){
                        randomDatalist.addAll(helperList);
                        randomDatalist.addAll(helperList1);
                    }else if(helperList1 != null && helperList != null && helperList.size() < helperList1.size()){
                        randomDatalist.addAll(helperList1);
                        randomDatalist.addAll(helperList);
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Random random = new Random();
                            int k = random.nextInt(randomDatalist.size()+1);
                            randomResult = randomDatalist.get(k);
                            Intent intent = new Intent(ActorDetailActivity.this, DetailActivity.class);
                            intent.putExtra(Constants.ExtrasKey.ID , randomResult.getId());
                            intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , randomResult.getFirst_air_date());
                            startActivity(intent);
                        }
                    });
                }
            }
        });
    }
}
