package mk.codeacademy.movieapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.gc.materialdesign.views.ButtonFlat;

import mk.codeacademy.movieapp.MainActivity;
import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.adapter.BoardingPagerAdapter;
import mk.codeacademy.movieapp.common.Constants;

public class BoardingActivity extends AppCompatActivity {

    BoardingPagerAdapter adapter;
    SharedPreferences sharedPreferences;

    ButtonFlat skip_finish_button;

    ViewPager boardingViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boarding);

        boardingViewPager = findViewById(R.id.boardingViewPager);
        skip_finish_button = findViewById(R.id.skip_finish_button);

        adapter = new BoardingPagerAdapter(getSupportFragmentManager() , 3);
        boardingViewPager.setAdapter(adapter);

        skip_finish_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (boardingViewPager.getCurrentItem() == 2){
                    finishOnBoarding();
                }else {
                    boardingViewPager.setCurrentItem(boardingViewPager.getCurrentItem()+1);
                }
            }
        });
    }

    void finishOnBoarding(){
        Intent main = new Intent(this , MainActivity.class);
        startActivity(main);
        finish();
    }
}
