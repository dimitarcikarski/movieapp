package mk.codeacademy.movieapp.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import mk.codeacademy.movieapp.fragment.BoardingFragmentOne;
import mk.codeacademy.movieapp.fragment.BoardingFragmentThree;
import mk.codeacademy.movieapp.fragment.BoardingFragmentTwo;

public class BoardingPagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public BoardingPagerAdapter(FragmentManager fm , int numOfTabs) {
        super(fm);
        this.mNumOfTabs = numOfTabs;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return BoardingFragmentOne.newInstance();
            case 1: return BoardingFragmentTwo.newInstance();
            case 2: return BoardingFragmentThree.newInstance();
            default: return null;
        }
    }
}
