package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnHistoryClick;
import mk.codeacademy.movieapp.model.db_models.History;

public class RecentAdapter extends RecyclerView.Adapter<RecentAdapter.RecentViewHolder> {

    List<History> data;
    LayoutInflater inflater;
    Context context;
    OnHistoryClick listener;

    public RecentAdapter(Context context , List<History> data , OnHistoryClick listener){
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_recent , parent , false);
        return new RecentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecentViewHolder holder, int position) {
        History history = data.get(position);
        holder.textView.setText(history.getTitle());
        String imgUrl = Constants.ImgUrls.POSTER_PATH + history.getPoster_path();
        if (history.getMedia_Type() == Constants.TypeOfPicture.MOVIE || history.getMedia_Type() == Constants.TypeOfPicture.TV) {
            Glide
                    .with(context)
                    .load(imgUrl)
                    .placeholder(R.drawable.ic_movie_placeholder)
                    .error(R.drawable.ic_movie_placeholder)
                    .centerCrop()
                    .into(holder.imageView);
        }else {
            Glide
                    .with(context)
                    .load(imgUrl)
                    .placeholder(R.drawable.ic_person_placeholder)
                    .error(R.drawable.ic_person_placeholder)
                    .centerCrop()
                    .into(holder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class RecentViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textView;

        public RecentViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.recent_poster);
            textView = itemView.findViewById(R.id.recent_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onHistoryClick(data.get(getAdapterPosition()));
                }
            });
        }
    }
}
