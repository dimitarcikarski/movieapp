package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnMediaItemClick;
import mk.codeacademy.movieapp.model.discover_movie.MediaResponseModel;

public class MovieSectionAdapter extends RecyclerView.Adapter<MovieSectionAdapter.MovieSectionViewHolder>{

    ArrayList<MediaResponseModel> data;
    LayoutInflater inflater;
    Context context;
    int typeOfView;
    OnMediaItemClick listener;
    int typeofsection;
    int mediaType;

    public MovieSectionAdapter(Context context , ArrayList<MediaResponseModel> data , OnMediaItemClick listener,int typeofsection , int mediaType){
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener  = listener;
        this.typeofsection = typeofsection;
        this.mediaType = mediaType;
    }

    @NonNull
    @Override
    public MovieSectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_movie_section , parent , false);
        return new MovieSectionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieSectionViewHolder holder, int position) {
        MediaResponseModel mediaResponseModel = data.get(position);


        if (mediaResponseModel != null) {
            final MovieBigItemAdapter bigItemAdapter = new MovieBigItemAdapter(context, mediaResponseModel.getResults(), typeOfView, listener);
            final MovieItemAdapter itemAdapter = new MovieItemAdapter(context, mediaResponseModel.getResults(), listener, typeofsection);

            switch (mediaResponseModel.getSectionView()) {
                case Constants.TypeOfView.NORMAL_VIEW:
                    LinearLayoutManager bLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                    holder.recyclerView.setLayoutManager(bLayoutManager);
                    holder.recyclerView.setAdapter(itemAdapter);
                    break;
                case Constants.TypeOfView.NORMAL_VIEW_SPAN_TWO:
                    GridLayoutManager ntwoLayoutManager = new GridLayoutManager(context, 2, LinearLayoutManager.HORIZONTAL, false);
                    holder.recyclerView.setLayoutManager(ntwoLayoutManager);
                    holder.recyclerView.setAdapter(itemAdapter);
                    break;
                case Constants.TypeOfView.BIG_VIEW:
                    GridLayoutManager sLayoutManager = new GridLayoutManager(context, 2, LinearLayoutManager.HORIZONTAL, false);
                    sLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int position) {
                            switch (bigItemAdapter.getItemViewType(position)) {
                                case MovieBigItemAdapter.TYPE_FIRST_ITEM:
                                    return 2;
                                case MovieBigItemAdapter.TYPE_ITEM:
                                    return 1;
                                default:
                                    return -1;
                            }
                        }
                    });
                    holder.recyclerView.setLayoutManager(sLayoutManager);
                    holder.recyclerView.setAdapter(bigItemAdapter);
                    break;
            }
            if (mediaResponseModel.getBackgroundType() == Constants.BackgroundType.BLACK){
                holder.sectionTitle.setTextColor(context.getResources().getColor(R.color.main_text_color));
            }else {
                holder.sectionTitle.setTextColor(context.getResources().getColor(R.color.main_text_color_black));
            }

            if (typeofsection == Constants.TypeOfSection.SIMILAR){
                holder.seeAll.setVisibility(View.GONE);
            }
            if (mediaResponseModel.getResults().size() <= 0){
                holder.sectionTitle.setVisibility(View.GONE);
            }else {
                holder.sectionTitle.setText(mediaResponseModel.getSectionTitle());
            }
            if (mediaType == Constants.TypeOfPicture.MOVIE && typeofsection == Constants.MovieSectionType.TOP_RATED ){
                mediaResponseModel.getResults().remove(0);
            }
        }


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MovieSectionViewHolder extends RecyclerView.ViewHolder {
        TextView sectionTitle , seeAll;
        RecyclerView recyclerView;

        public MovieSectionViewHolder(@NonNull View itemView) {
            super(itemView);

            sectionTitle = itemView.findViewById(R.id.section_title);
            seeAll = itemView.findViewById(R.id.section_seeall);
            recyclerView = itemView.findViewById(R.id.movie_item_recyclerview);

            seeAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onSeeAllClick(data.get(getAdapterPosition()) , mediaType);

                }
            });

        }
    }
}
