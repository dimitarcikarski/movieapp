package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnCastClick;
import mk.codeacademy.movieapp.model.movie_credits.Cast;

public class CastAdapter extends RecyclerView.Adapter<CastAdapter.CastViewHolder> {

    ArrayList<Cast> data;
    LayoutInflater inflater;
    Context context;
    OnCastClick listener;

    public CastAdapter(Context context , ArrayList<Cast> data, OnCastClick listener){
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener  = listener;
    }

    @NonNull
    @Override
    public CastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_cast , parent , false);
        return new CastViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CastViewHolder holder, int position) {
        Cast cast = data.get(position);
        holder.cast_name.setText(cast.getName());
        holder.cast_character.setText(cast.getCharacter());

        String imgUrl = Constants.ImgUrls.POSTER_PATH + cast.getProfile_path();
        Glide
                .with(context)
                .load(imgUrl)
                .placeholder(R.drawable.ic_person_placeholder)
                .error(R.drawable.ic_person_placeholder)
                .centerCrop()
                .into(holder.cast_pp);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class CastViewHolder extends RecyclerView.ViewHolder {

        ImageView cast_pp;
        TextView cast_name;
        TextView cast_character;

        public CastViewHolder(@NonNull View itemView) {
            super(itemView);

            cast_pp = itemView.findViewById(R.id.cast_profile_picture);
            cast_name = itemView.findViewById(R.id.cast_name);
            cast_character = itemView.findViewById(R.id.cast_character);

            cast_pp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCastClick(data.get(getAdapterPosition()));
                }
            });
        }
    }
}
