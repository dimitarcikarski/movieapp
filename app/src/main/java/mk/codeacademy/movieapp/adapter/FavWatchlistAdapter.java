package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnFavMovieClick;
import mk.codeacademy.movieapp.interfaces.OnWatchlistClick;
import mk.codeacademy.movieapp.model.db_models.FavTvShows;
import mk.codeacademy.movieapp.model.db_models.Watchlist;

public class FavWatchlistAdapter extends RecyclerView.Adapter<FavWatchlistAdapter.FavWatchListViewHolder> {

    List<Watchlist> data;
    LayoutInflater inflater;
    Context context;
    OnWatchlistClick listener;

    public FavWatchlistAdapter(Context context ,List<Watchlist> data , OnWatchlistClick listener){
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.context =context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public FavWatchListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_fav,parent , false);
        return new FavWatchListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavWatchListViewHolder holder, int position) {
        Watchlist watchlist = data.get(position);
        holder.title.setText(watchlist.getTitle());
        String imgUrl = Constants.ImgUrls.POSTER_PATH + watchlist.getPoster_path();
        Glide
                .with(context)
                .load(imgUrl)
                .placeholder(R.drawable.ic_movie_placeholder)
                .error(R.drawable.ic_movie_placeholder)
                .centerCrop()
                .into(holder.poster);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class FavWatchListViewHolder extends RecyclerView.ViewHolder {

        ImageView poster;
        TextView title;

        public FavWatchListViewHolder(@NonNull View itemView) {
            super(itemView);

            poster = itemView.findViewById(R.id.fav_poster);
            title = itemView.findViewById(R.id.fav_movie_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onWatchlistClick(data.get(getAdapterPosition()));
                }
            });
        }
    }
}
