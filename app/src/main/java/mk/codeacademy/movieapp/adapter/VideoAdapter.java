package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnMediaClick;
import mk.codeacademy.movieapp.interfaces.OnMediaItemClick;
import mk.codeacademy.movieapp.model.videos.VideoResults;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoViewHolder> {

    ArrayList<VideoResults> data;
    LayoutInflater inflater;
    Context context;
    OnMediaClick listener;

    public VideoAdapter(Context context , ArrayList<VideoResults> data,  OnMediaClick listener){
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public VideoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_video , parent , false);
        return new VideoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoViewHolder holder, int position) {
        VideoResults videoResults = data.get(position);
        holder.textView.setText(videoResults.getName());
        holder.type.setText(videoResults.getType());
        String imgUrl = Constants.ImgUrls.YOUTUBE_THUMBNAIL + videoResults.getKey()+ Constants.ImgUrls.YOUTUBE_THUMBNAIL_QUALITY;
        Glide
                .with(context)
                .load(imgUrl)
                .centerCrop()
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class VideoViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        TextView type;
        ImageView imageView;

        public VideoViewHolder(@NonNull View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.detail_video_item_title);
            imageView = itemView.findViewById(R.id.detail_video_image);
            type = itemView.findViewById(R.id.video_type);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onVideoClick(getAdapterPosition());
                }
            });
        }
    }
}
