package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnMediaItemClick;
import mk.codeacademy.movieapp.interfaces.OnPersonClick;
import mk.codeacademy.movieapp.model.people.PeopleResult;

public class PeopleAdapter extends RecyclerView.Adapter<PeopleAdapter.PeopleViewHolder> {
    ArrayList<PeopleResult> data;
    LayoutInflater inflater;
    Context context;
    OnPersonClick listener;

    public PeopleAdapter(Context context , ArrayList<PeopleResult> data , OnPersonClick listener){
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public PeopleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_people , parent , false);
        return new PeopleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PeopleViewHolder holder, int position) {
        PeopleResult peopleResult = data.get(position);
        holder.people_name.setText(peopleResult.getName());
        holder.people_number.setText("#"+(position+1));

        String imgUrl = Constants.ImgUrls.POSTER_PATH + peopleResult.getProfile_path();
        Glide
                .with(context)
                .load(imgUrl)
                .placeholder(R.drawable.ic_person_placeholder)
                .error(R.drawable.ic_person_placeholder)
                .centerCrop()
                .into(holder.people_pp);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class PeopleViewHolder extends RecyclerView.ViewHolder {

        ImageView people_pp;
        TextView people_name;
        TextView people_number;

        public PeopleViewHolder(@NonNull View itemView) {
            super(itemView);

            people_pp = itemView.findViewById(R.id.people_profile_picture);
            people_name = itemView.findViewById(R.id.people_name);
            people_number = itemView.findViewById(R.id.people_number);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onPersonClick(data.get(getAdapterPosition()));
                }
            });
        }
    }
}
