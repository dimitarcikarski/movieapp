package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnClickSeeAll;
import mk.codeacademy.movieapp.model.discover_movie.MovieResults;


public class SeeAllAdapter extends RecyclerView.Adapter<SeeAllAdapter.SeeAllViewHolder> {

    ArrayList<MovieResults> data;
    LayoutInflater inflater;
    Context context;
    OnClickSeeAll listener;
    int mediaType;

    public SeeAllAdapter(Context context, ArrayList<MovieResults> data, OnClickSeeAll listener,int mediaType){
        this.data = data;
        this.inflater=LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
        this.mediaType = mediaType;
    }

    @NonNull
    @Override
    public SeeAllViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_search,parent, false);
        return new SeeAllViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SeeAllViewHolder holder, int position) {
        MovieResults results = data.get(position);
        String imgurl = "";
        switch (mediaType) {
            case Constants.TypeOfPicture.MOVIE:
                holder.title_name.setText(results.getTitle());
                if (results.getRelease_date() != null) {
                    if (results.getRelease_date().length() > 3) {
                        holder.year_job_type.setText(results.getRelease_date().substring(0, 4) + "");
                    }
                }
                imgurl = Constants.ImgUrls.POSTER_PATH + results.getPoster_path();
                break;
            case Constants.TypeOfPicture.TV:
                holder.title_name.setText(results.getName());
                if (results.getFirst_air_date() != null) {
                    if (results.getFirst_air_date().length() > 3 ) {
                        holder.year_job_type.setText(results.getFirst_air_date().substring(0, 4) + "");
                    }
                }
                imgurl = Constants.ImgUrls.POSTER_PATH + results.getPoster_path();
                break;
            case Constants.TypeOfPicture.GENRE:
                holder.title_name.setText(results.getTitle());
                if (results.getRelease_date() != null) {
                    if (results.getRelease_date().length() > 3) {
                        holder.year_job_type.setText(results.getRelease_date().substring(0, 4) + "");
                    }
                }
                imgurl = Constants.ImgUrls.POSTER_PATH + results.getPoster_path();
                break;

        }
        holder.vote_average.setText(results.getVote_average()+"");
        Glide
                .with(context)
                .load(imgurl)
                .placeholder(R.drawable.ic_movie_placeholder)
                .error(R.drawable.ic_movie_placeholder)
                .centerCrop()
                .into(holder.search_image);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class SeeAllViewHolder extends RecyclerView.ViewHolder {

        ImageView search_image;
        TextView title_name , year_job_type;
        TextView vote_average;

        public SeeAllViewHolder(@NonNull View itemView) {
            super(itemView);

            search_image = itemView.findViewById(R.id.search_image);
            title_name = itemView.findViewById(R.id.search_name_title);
            year_job_type = itemView.findViewById(R.id.search_year_job_type);
            vote_average = itemView.findViewById(R.id.search_known_for_acting);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClickSeeAll(data.get(getAdapterPosition()));
                }
            });
        }
    }
}
