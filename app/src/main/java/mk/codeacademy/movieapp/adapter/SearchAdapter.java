package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnSearchItemClick;
import mk.codeacademy.movieapp.model.search.SearchKnownFor;
import mk.codeacademy.movieapp.model.search.SearchResults;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> {

    ArrayList<SearchResults> data;
    ArrayList<SearchKnownFor> knownData;
    LayoutInflater inflater;
    Context context;
    OnSearchItemClick listener;

    public SearchAdapter(Context context , ArrayList<SearchResults> data , OnSearchItemClick listener){
        this.data = data;
        this.context =context;
        this.inflater = LayoutInflater.from(context);
        this.listener  = listener;
    }

    @NonNull
    @Override
    public SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_search , parent , false);
        return new SearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {
        SearchResults results = data.get(position);
        SearchKnownFor searchKnownFor = new SearchKnownFor();
        if (results.getMedia_type().equals(Constants.Media_Type.PERSON)){
            knownData = results.getKnown_for();
            if (knownData.size() > 0)
            searchKnownFor = knownData.get(0);
        }
        String imgurl = "";
        if (results.getMedia_type() != null) {
            switch (results.getMedia_type()) {
                case Constants.Media_Type.MOVIE:
                    holder.title_name.setText(results.getTitle());
                    if (results.getRelease_date() != null) {
                        if (results.getRelease_date().length() > 3) {
                            holder.year_job_type.setText(results.getRelease_date().substring(0, 4) + " | MOVIE");
                        }
                    }
                    imgurl = Constants.ImgUrls.POSTER_PATH + results.getPoster_path();
                    Glide
                            .with(context)
                            .load(imgurl)
                            .placeholder(R.drawable.ic_movie_placeholder)
                            .error(R.drawable.ic_movie_placeholder)
                            .centerCrop()
                            .into(holder.search_image);
                    break;
                case Constants.Media_Type.TV:
                    holder.title_name.setText(results.getName());
                    if (results.getFirst_air_date() != null) {
                        if (results.getFirst_air_date().length() > 3 ) {
                            holder.year_job_type.setText(results.getFirst_air_date().substring(0, 4) + " | TV - Show");
                        }
                    }
                    imgurl = Constants.ImgUrls.POSTER_PATH + results.getPoster_path();
                    Glide
                            .with(context)
                            .load(imgurl)
                            .placeholder(R.drawable.ic_movie_placeholder)
                            .error(R.drawable.ic_movie_placeholder)
                            .centerCrop()
                            .into(holder.search_image);
                    break;
                case Constants.Media_Type.PERSON:
                    holder.title_name.setText(results.getName());
                    holder.year_job_type.setText(results.getKnown_for_department());
                    holder.known_for_acting.setText(searchKnownFor.getTitle());
                    imgurl = Constants.ImgUrls.POSTER_PATH + results.getProfile_path();
                    Glide
                            .with(context)
                            .load(imgurl)
                            .placeholder(R.drawable.ic_person_placeholder)
                            .error(R.drawable.ic_person_placeholder)
                            .centerCrop()
                            .into(holder.search_image);
                    break;

            }
        }


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class SearchViewHolder extends RecyclerView.ViewHolder {

        ImageView search_image;
        TextView title_name , year_job_type;
        TextView known_for_acting;

        public SearchViewHolder(@NonNull View itemView) {
            super(itemView);

            search_image = itemView.findViewById(R.id.search_image);
            title_name = itemView.findViewById(R.id.search_name_title);
            year_job_type = itemView.findViewById(R.id.search_year_job_type);
            known_for_acting = itemView.findViewById(R.id.search_known_for_acting);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(data.get(getAdapterPosition()));
                }
            });
        }
    }
}
