package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.interfaces.OnSearchItemClick;
import mk.codeacademy.movieapp.model.db_models.RecentSearch;

public class RecentSearchAdapter extends RecyclerView.Adapter<RecentSearchAdapter.RecentSearchViewHolder> {

    List<RecentSearch> data;
    LayoutInflater inflater;
    OnSearchItemClick listener;

    public RecentSearchAdapter(Context context , List<RecentSearch> data ,OnSearchItemClick listener){
        this.data = data;
        this.inflater =LayoutInflater.from(context);
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecentSearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_recentsearch,parent , false);
        return new RecentSearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecentSearchViewHolder holder, int position) {
        RecentSearch recentSearch = data.get(position);
        holder.textView.setText(recentSearch.getTitle());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class RecentSearchViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        public RecentSearchViewHolder(@NonNull View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.recent_search_textview);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRecentSearchClick(data.get(getAdapterPosition()));
                }
            });
        }
    }
}
