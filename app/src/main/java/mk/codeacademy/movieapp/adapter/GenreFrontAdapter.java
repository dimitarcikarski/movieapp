package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnGenreClick;
import mk.codeacademy.movieapp.model.movie_details.Genre;

public class GenreFrontAdapter extends RecyclerView.Adapter<GenreFrontAdapter.GenreFrontViewHolder> {

    ArrayList<Genre> data;
    LayoutInflater inflater;
    OnGenreClick listener;
    Context context;

    public GenreFrontAdapter(Context context , ArrayList<Genre> data ,OnGenreClick listener ){
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.listener = listener;
        this.context = context;
    }

    @NonNull
    @Override
    public GenreFrontViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_genre_front_page , parent , false);
        return new GenreFrontViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GenreFrontViewHolder holder, int position) {
        Genre genre = data.get(position);
        String url;
        holder.textView.setText(genre.getName());
        switch (genre.getId()){
            case Constants
                    .Genres.ACTION:
                url = "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/76f4d707-7a3d-4f1b-b7fd-9109b1c07ae0/daty7wi-aece5298-6c48-401e-ab5a-cf2307ca89f4.jpg/v1/fill/w_500,h_800,q_75,strp/background_for_covers_of_action_by_jess104_daty7wi-fullview.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7ImhlaWdodCI6Ijw9ODAwIiwicGF0aCI6IlwvZlwvNzZmNGQ3MDctN2EzZC00ZjFiLWI3ZmQtOTEwOWIxYzA3YWUwXC9kYXR5N3dpLWFlY2U1Mjk4LTZjNDgtNDAxZS1hYjVhLWNmMjMwN2NhODlmNC5qcGciLCJ3aWR0aCI6Ijw9NTAwIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmltYWdlLm9wZXJhdGlvbnMiXX0.eKjafXuOYux2tfAtB3P77vhzJ4vc8fmX7_lQclHAE-k";
                break;
            case Constants
                    .Genres.ADVENTURE:
                url = "https://www.thedesignwork.com/wp-content/uploads/2010/08/The-Design-Work-Antique-Maps-1.jpg";
                break;
            case Constants
                    .Genres.ANIMATED:
                url = "https://www.thedesignwork.com/wp-content/uploads/2010/08/The-Design-Work-Antique-Maps-1.jpg";
                break;
            case Constants
                    .Genres.COMEDY:
                url = "https://images-na.ssl-images-amazon.com/images/S/sgp-catalog-images/region_US/w6dww-850019162-Full-Image_GalleryBackground-en-US-1526539932984._SX1080_.jpg";
                break;
            case Constants
                    .Genres.CRIME:
                url = "https://i1.wp.com/www.icon0.com/wp-content/uploads/2019/03/45574462324_082bc66be2_z.jpg?resize=640%2C453";
                break;
            case Constants
                    .Genres.DOCUMENTARY:
                url = "https://img.freepik.com/free-vector/yellow-background-with-movie-projector_18591-8617.jpg?size=626&ext=jpg";
                break;
            case Constants
                    .Genres.DRAMA:
                url = "https://ak0.picdn.net/shutterstock/videos/688390/thumb/4.jpg";
                break;
            case Constants
                    .Genres.FAMILY:
                url = "https://image.freepik.com/free-vector/green-background-happy-family-having-fun-playing-field_9634-41.jpg";
                break;
            case Constants
                    .Genres.FANTASY:
                url = "https://png.pngtree.com/thumb_back/fw800/background/20190223/ourmid/pngtree-purple-fantasy-forest-background-design-backgroundwoods-backgroundfireflyillustration-backgroundadvertising-image_67876.jpg";
                break;
            case Constants
                    .Genres.HISTORY:
                url = "https://img.freepik.com/free-photo/elevated-view-clock-paper-pen-ink-bottle-map_23-2147837183.jpg?size=626&ext=jpg";
                break;
            case Constants
                    .Genres.HORROR:
                url = "https://png.pngtree.com/thumb_back/fw800/background/20190223/ourmid/pngtree-dark-halloween-hand-drawn-horror-background-design-backgroundhand-painted-backgroundhalloween-image_70579.jpg";
                break;
            case Constants
                    .Genres.MUSIC:
                url = "https://i.pinimg.com/originals/73/06/65/730665c249e603d3260e0a9fdb2d5ca8.jpg";
                break;
            case Constants
                    .Genres.MYSTERY:
                url = "https://i.pinimg.com/originals/47/b2/92/47b29232ee6013faa4afb02ba1d3d138.png";
                break;
            case Constants
                    .Genres.ROMANCE:
                url = "https://img.freepik.com/free-photo/3d-silhouette-couple-kissing-against-sunset-ocean-landscape_1048-9562.jpg?size=626&ext=jpg";
                break;
            case Constants
                    .Genres.SCI_FI:
                url = "https://i.pinimg.com/236x/cb/a1/63/cba1638147f1c36f15925aa45480bf4e.jpg";
                break;
            case Constants
                    .Genres.THRILLER:
                url = "https://ak4.picdn.net/shutterstock/videos/7952224/thumb/2.jpg";
                break;
            case Constants
                    .Genres.TV_MOVIE:
                url = "https://png.pngtree.com/thumb_back/fw800/back_our/20190622/ourmid/pngtree-film-and-television-film-festival-retro-wind-camera-film-poster-image_215411.jpg";
                break;
            case Constants
                    .Genres.WAR:
                url = "https://cdn-images.threadless.com/threadless-shop/products/2015/768x768design_01.jpg?w=1272&h=920";
                break;
            case Constants
                    .Genres.WESTERN:
                url = "https://thumbs.dreamstime.com/b/western-style-wood-background-brown-56422507.jpg";
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + genre.getId());
        }
        Glide
                .with(context)
                .load(url)
                .placeholder(R.drawable.ic_movie_placeholder)
                .error(R.drawable.ic_movie_placeholder)
                .centerCrop()
                .into(holder.genre_background);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class GenreFrontViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        ImageView genre_background;

        public GenreFrontViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.genre_front_name);
            genre_background = itemView.findViewById(R.id.genre_background);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onGenreClick(data.get(getAdapterPosition()));
                }
            });
        }
    }
}
