package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnMediaClick;
import mk.codeacademy.movieapp.interfaces.Transfer;
import mk.codeacademy.movieapp.model.videos.VideoResults;

public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.VideoListViewHolder> {

    ArrayList<VideoResults> data;
    LayoutInflater inflater;
    Context context;
    OnMediaClick listener;
    Transfer transfer;
    int pos;

    public VideoListAdapter(Context context , ArrayList<VideoResults> data,  OnMediaClick listener , int pos , Transfer transfer){
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
        this.pos = pos;
        this.transfer = transfer;
    }

    @NonNull
    @Override
    public VideoListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_videolist , parent , false);
        return new VideoListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoListViewHolder holder, int position) {
        VideoResults results = data.get(position);
        holder.video_title_list.setText(results.getName());
        holder.video_type_list.setText(results.getType());

        String imgUrl = Constants.ImgUrls.YOUTUBE_THUMBNAIL + results.getKey()+ Constants.ImgUrls.YOUTUBE_THUMBNAIL_QUALITY;
        Glide
                .with(context)
                .load(imgUrl)
                .centerCrop()
                .into(holder.video_image_list);
        if (pos == position){
            holder.video_title_list.setTextColor(context.getResources().getColor(R.color.black));
            holder.video_list_background.setBackgroundColor(context.getResources().getColor(R.color.white));
            transfer.key(results.getKey());
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class VideoListViewHolder extends RecyclerView.ViewHolder {

        TextView video_title_list , video_type_list;
        ImageView video_image_list;
        LinearLayout video_list_background;

        public VideoListViewHolder(@NonNull View itemView) {
            super(itemView);
            video_image_list = itemView.findViewById(R.id.video_image_list);
            video_title_list = itemView.findViewById(R.id.video_title_list);
            video_type_list = itemView.findViewById(R.id.video_type_list);
            video_list_background = itemView.findViewById(R.id.video_list_background);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onVideoClick(getAdapterPosition());
                }
            });
        }
    }
}
