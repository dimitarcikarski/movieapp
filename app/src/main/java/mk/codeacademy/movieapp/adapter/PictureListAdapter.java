package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnPictureClick;
import mk.codeacademy.movieapp.interfaces.TransferPicture;
import mk.codeacademy.movieapp.model.pictures.Backdrop;

public class PictureListAdapter extends RecyclerView.Adapter<PictureListAdapter.PictureListViewHolder> {

    List<Backdrop> data;
    LayoutInflater inflater;
    Context context;
    OnPictureClick listener;
    int position1;
    TransferPicture transferPicture;

    public PictureListAdapter(Context context , List<Backdrop> data ,OnPictureClick listener,int position1 ,TransferPicture transferPicture ){
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
        this.position1 = position1;
        this.transferPicture = transferPicture;
    }

    @NonNull
    @Override
    public PictureListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_imagelist, parent , false);
        return new PictureListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PictureListViewHolder holder, int position) {
        Backdrop backdrop = data.get(position);
        String imgUrl = Constants.ImgUrls.POSTER_PATH + backdrop.getFile_path();
        Glide
                .with(context)
                .load(imgUrl)
                .centerCrop()
                .into(holder.imageView);
        if (position1 == position){
            transferPicture.picturePosition(position);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class PictureListViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        public PictureListViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image_imagelist);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onPictureClick(getAdapterPosition());
                }
            });
        }
    }
}
