package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnFavMovieClick;
import mk.codeacademy.movieapp.model.db_models.FavMovie;

public class FavMovieAdapter extends RecyclerView.Adapter<FavMovieAdapter.FavViewHolder> {

    List<FavMovie> data;
    LayoutInflater inflater;
    Context context;
    OnFavMovieClick listener;

    public FavMovieAdapter(Context context, List<FavMovie> data , OnFavMovieClick listener){
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.context =context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public FavViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_fav,parent , false);
        return new FavViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavViewHolder holder, int position) {
        FavMovie favMovie = data.get(position);
        holder.title.setText(favMovie.getTitle());
        String imgUrl = Constants.ImgUrls.POSTER_PATH + favMovie.getPoster_path();
        Glide
                .with(context)
                .load(imgUrl)
                .placeholder(R.drawable.ic_movie_placeholder)
                .error(R.drawable.ic_movie_placeholder)
                .centerCrop()
                .into(holder.poster);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class FavViewHolder extends RecyclerView.ViewHolder {

        ImageView poster;
        TextView title;

        public FavViewHolder(@NonNull View itemView) {
            super(itemView);

            poster = itemView.findViewById(R.id.fav_poster);
            title = itemView.findViewById(R.id.fav_movie_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onMovieClick(data.get(getAdapterPosition()));
                }
            });
        }
    }
}
