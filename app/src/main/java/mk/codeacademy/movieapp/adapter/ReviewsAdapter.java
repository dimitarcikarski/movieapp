package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.model.reviews.ReviewResults;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ReviewsViewHolder> {

    ArrayList<ReviewResults> data;
    LayoutInflater inflater;

    private int clickedTextViewPos=-1;
    private int lastclickedTextViewPos=-1;

    public ReviewsAdapter(Context context , ArrayList<ReviewResults> data){
        this.data = data;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ReviewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_review,parent , false);
        return new ReviewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewsViewHolder holder, int position) {
        ReviewResults reviewResults = data.get(position);
        holder.author.setText(reviewResults.getAuthor());
        holder.content.setText(reviewResults.getContent());

        if (clickedTextViewPos == position) {
            reviewResults.setChecked(true);
        }
        if(clickedTextViewPos == lastclickedTextViewPos){
            reviewResults.setChecked(false);
        }else{
            reviewResults.setChecked(false);
        }

        if (reviewResults.getChecked()){
            holder.content.setMaxLines(100);
        }else {
            holder.content.setMaxLines(4);
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ReviewsViewHolder extends RecyclerView.ViewHolder {

        TextView author , content;

        public ReviewsViewHolder(@NonNull View itemView) {
            super(itemView);
            author = itemView.findViewById(R.id.reviews_author);
            content = itemView.findViewById(R.id.reviews_content);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickedTextViewPos = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });
        }

    }
}