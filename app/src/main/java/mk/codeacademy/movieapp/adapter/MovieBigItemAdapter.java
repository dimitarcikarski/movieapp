package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnMediaItemClick;
import mk.codeacademy.movieapp.model.discover_movie.MovieResults;

public class MovieBigItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_FIRST_ITEM = 0;
    public static final int TYPE_ITEM = 1;

    ArrayList<MovieResults> data;
    LayoutInflater inflater;
    Context context;
    int typeOfView;
    OnMediaItemClick listener;


    public MovieBigItemAdapter(Context context, ArrayList<MovieResults> data , int typeOfView ,  OnMediaItemClick listener){
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.typeOfView = typeOfView;
        this.listener = listener;
    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_FIRST_ITEM:
                final View view = inflater.inflate(R.layout.item_big_movie, parent, false);
                return new BigViewHolder(view);
            case TYPE_ITEM:
                final View view2 = inflater.inflate(R.layout.item_big_horiz, parent, false);
                return new NormalViewHolder(view2);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MovieResults movieResults = data.get(position);
        switch (holder.getItemViewType()) {
            case TYPE_FIRST_ITEM:
                BigViewHolder bigViewHolder = (BigViewHolder) holder;
                String imgUrl = Constants.ImgUrls.POSTER_PATH + movieResults.getPoster_path();
                Glide
                        .with(context)
                        .load(imgUrl)
                        .placeholder(R.drawable.ic_movie_placeholder)
                        .error(R.drawable.ic_movie_placeholder)
                        .centerCrop()
                        .into(bigViewHolder.big_poster);

                if (movieResults.getFirst_air_date() == null) {
                    bigViewHolder.bigtitle.setText(movieResults.getTitle());
                }else {
                    bigViewHolder.bigtitle.setText(movieResults.getName());
                }
                break;
            case TYPE_ITEM:
                NormalViewHolder normalViewHolder = (NormalViewHolder) holder;
                String bigimgUrl = Constants.ImgUrls.POSTER_PATH + movieResults.getBackdrop_path();
                Glide
                        .with(context)
                        .load(bigimgUrl)
                        .placeholder(R.drawable.ic_movie_placeholder)
                        .error(R.drawable.ic_movie_placeholder)
                        .centerCrop()
                        .into(normalViewHolder.poster);
                if (movieResults.getFirst_air_date() == null) {
                    normalViewHolder.smalltitle.setText(movieResults.getTitle());
                }else {
                    normalViewHolder.smalltitle.setText(movieResults.getName());
                }

                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
                return TYPE_FIRST_ITEM;
            } else return TYPE_ITEM;

    }

    @Override
    public int getItemCount() {
        return data.size();
    }



    final class NormalViewHolder extends RecyclerView.ViewHolder {
        ImageView poster;
        TextView smalltitle;
        TextView vote;
        public NormalViewHolder(View itemView) {
            super(itemView);

            poster = itemView.findViewById(R.id.big_poster_horizontal);
            smalltitle = itemView.findViewById(R.id.textview_smallVH);
            //vote = itemView.findViewById(R.id.vote);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(data.get(getAdapterPosition()));
                }
            });
        }
    }

    final class BigViewHolder extends RecyclerView.ViewHolder {

        ImageView big_poster;
        TextView bigtitle;
        public BigViewHolder(View itemView) {
            super(itemView);

            big_poster = itemView.findViewById(R.id.big_poster);
            bigtitle = itemView.findViewById(R.id.textview_bigVH);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(data.get(getAdapterPosition()));
                }
            });
        }
    }
}
