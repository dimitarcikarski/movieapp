package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnMediaItemClick;
import mk.codeacademy.movieapp.model.movie_credits.KnownForCast;

public class KnownForAdapter extends RecyclerView.Adapter<KnownForAdapter.KnownForViewHolder> {

    ArrayList<KnownForCast> data;
    LayoutInflater inflater;
    Context context;
    OnMediaItemClick listener;

    public KnownForAdapter(Context context ,ArrayList<KnownForCast> data , OnMediaItemClick listener){
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.listener = listener;
        this.context = context;
    }

    @NonNull
    @Override
    public KnownForViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_known_for, parent , false);
        return new KnownForViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull KnownForViewHolder holder, int position) {
        KnownForCast knownForCast = data.get(position);
        holder.known_for_title.setText(knownForCast.getTitle());

        holder.known_for_title.setText(knownForCast.getTitle());
        String imgUrl = Constants.ImgUrls.POSTER_PATH + knownForCast.getPoster_path();
        Glide
                .with(context)
                .load(imgUrl)
                .placeholder(R.drawable.ic_movie_placeholder)
                .error(R.drawable.ic_movie_placeholder)
                .centerCrop()
                .into(holder.poster);
        if (knownForCast.getJob() != null){
            holder.job.setText(knownForCast.getJob());
        }else {
            holder.job.setText("as");
            holder.job.setTextColor(context.getResources().getColor(R.color.gray));
            holder.character.setText(knownForCast.getCharacter());
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class KnownForViewHolder extends RecyclerView.ViewHolder {

        ImageView poster;
        TextView job , character;
        TextView known_for_title;
        public KnownForViewHolder(@NonNull View itemView) {
            super(itemView);

            poster = itemView.findViewById(R.id.knownforImage);
            job = itemView.findViewById(R.id.knownforjob);
            character = itemView.findViewById(R.id.knownforcharacter);
            known_for_title = itemView.findViewById(R.id.known_for_title);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onKnownForClick(data.get(getAdapterPosition()));
                }
            });
        }
    }
}
