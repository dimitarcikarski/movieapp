package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnCastClick;
import mk.codeacademy.movieapp.model.movie_credits.Cast;
import mk.codeacademy.movieapp.model.movie_credits.Crew;

public class CrewAdapter extends RecyclerView.Adapter<CrewAdapter.CrewViewHolder> {

    ArrayList<Crew> data;
    LayoutInflater inflater;
    Context context;
    OnCastClick listener;

    public CrewAdapter(Context context , ArrayList<Crew> data , OnCastClick listener){
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public CrewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_crew , parent , false);
        return new CrewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CrewViewHolder holder, int position) {
        Crew crew = data.get(position);
        holder.crew_name.setText(crew.getName());
        holder.crew_job.setText(crew.getJob());

        String imgUrl = Constants.ImgUrls.POSTER_PATH + crew.getProfile_path();
        Glide
                .with(context)
                .load(imgUrl)
                .placeholder(R.drawable.ic_person_placeholder)
                .error(R.drawable.ic_person_placeholder)
                .centerCrop()
                .into(holder.crew_pp);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class CrewViewHolder extends RecyclerView.ViewHolder {

        ImageView crew_pp;
        TextView crew_name;
        TextView crew_job;

        public CrewViewHolder(@NonNull View itemView) {
            super(itemView);

            crew_pp = itemView.findViewById(R.id.crew_profile_picture);
            crew_name = itemView.findViewById(R.id.crew_name);
            crew_job = itemView.findViewById(R.id.crew_job);

            crew_pp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onCrewClick(data.get(getAdapterPosition()));
                }
            });
        }
    }
}
