package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.model.tv_details.Seasons;

public class SeasonsAdapter extends RecyclerView.Adapter<SeasonsAdapter.SeasonsViewHolder> {

    ArrayList<Seasons> data;
    LayoutInflater inflater;
    Context context;

    public SeasonsAdapter(Context context , ArrayList<Seasons> data){
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @NonNull
    @Override
    public SeasonsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_season , parent , false);
        return new SeasonsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SeasonsViewHolder holder, int position) {
        Seasons seasons = data.get(position);
        holder.name.setText(seasons.getName());
        holder.season_number.setText(seasons.getSeason_number()+".");
        if (seasons.getAir_date() != null) {
            holder.year.setText(seasons.getAir_date().substring(0, 4));
        }
        holder.overview.setText(seasons.getOverview());
        String imgurl = Constants.ImgUrls.POSTER_PATH + seasons.getPoster_path();
            Glide
                    .with(context)
                    .load(imgurl)
                    .placeholder(R.drawable.ic_movie_placeholder)
                    .error(R.drawable.ic_movie_placeholder)
                    .centerCrop()
                    .into(holder.seasons_image);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class SeasonsViewHolder extends RecyclerView.ViewHolder {

        TextView name , year , overview , season_number;
        ImageView seasons_image;

        public SeasonsViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.season_name);
            year = itemView.findViewById(R.id.season_air_date);
            overview = itemView.findViewById(R.id.season_overview);
            seasons_image = itemView.findViewById(R.id.season_image);
            season_number = itemView.findViewById(R.id.season_number);
        }
    }
}
