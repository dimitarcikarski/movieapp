package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnMediaItemClick;
import mk.codeacademy.movieapp.model.discover_movie.MovieResults;

public class MovieItemAdapter extends RecyclerView.Adapter<MovieItemAdapter.MovieItemViewHolder> {

    ArrayList<MovieResults> data;
    LayoutInflater inflater;
    Context context;
    OnMediaItemClick listener;
    int typeofsection;

    public MovieItemAdapter(Context context, ArrayList<MovieResults> data , OnMediaItemClick listener , int typeofsection){
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
        this.typeofsection = typeofsection;
    }

    @NonNull
    @Override
    public MovieItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_movie,parent,false);
        return new MovieItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieItemViewHolder holder, int position) {
        MovieResults movieResults = data.get(position);
        /*if (typeOfPicture == Constants.TypeOfPicture.MOVIE) {
            holder.title.setText(movieResults.getTitle());
        }else {
            holder.title.setText(movieResults.getName());
        }*/
        String imgUrl = Constants.ImgUrls.POSTER_PATH + movieResults.getPoster_path();
        Glide
                .with(context)
                .load(imgUrl)
                .placeholder(R.drawable.ic_movie_placeholder)
                .error(R.drawable.ic_movie_placeholder)
                .centerCrop()
                .into(holder.poster);
        if (typeofsection == Constants.MovieSectionType.TOP_RATED) {
            holder.star_average.setVisibility(View.VISIBLE);
            holder.star_average_num.setText(movieResults.getVote_average() + "");
        }
        //holder.vote.setText(movieResults.getVote_average()+"");
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class MovieItemViewHolder extends RecyclerView.ViewHolder {

        ImageView poster;
        TextView star_average_num;
        RelativeLayout star_average;
        public MovieItemViewHolder(@NonNull View itemView) {
            super(itemView);
            poster = itemView.findViewById(R.id.poster);
            star_average_num = itemView.findViewById(R.id.star_average_num);
            star_average = itemView.findViewById(R.id.star_average);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(data.get(getAdapterPosition()));
                }
            });

        }
    }
}
