package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnFavMovieClick;
import mk.codeacademy.movieapp.model.db_models.FavTvShows;

public class FavTvShowAdapter extends RecyclerView.Adapter<FavTvShowAdapter.FavTvShowViewHolder> {

    List<FavTvShows> data;
    LayoutInflater inflater;
    Context context;
    OnFavMovieClick listener;

    public FavTvShowAdapter(Context context , List<FavTvShows> data , OnFavMovieClick listener){
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public FavTvShowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_fav,parent , false);
        return new FavTvShowViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavTvShowViewHolder holder, int position) {
        FavTvShows favTvShows = data.get(position);
        holder.textView.setText(favTvShows.getTitle());
        String imgUrl = Constants.ImgUrls.POSTER_PATH + favTvShows.getPoster_path();
        Glide
                .with(context)
                .load(imgUrl)
                .placeholder(R.drawable.ic_movie_placeholder)
                .error(R.drawable.ic_movie_placeholder)
                .centerCrop()
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class FavTvShowViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textView;

        public FavTvShowViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.fav_poster);
            textView = itemView.findViewById(R.id.fav_movie_name);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onTvClick(data.get(getAdapterPosition()));
                }
            });
        }
    }


}
