package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnMediaClick;
import mk.codeacademy.movieapp.interfaces.OnSliderClick;
import mk.codeacademy.movieapp.model.discover_movie.MovieResults;

public class SliderAdapter extends RecyclerView.Adapter<SliderAdapter.SliderViewHolder> {

    ArrayList<MovieResults> data;
    LayoutInflater inflater;
    Context context;
    OnSliderClick listener;
    public SliderAdapter(Context context , ArrayList<MovieResults> data ,OnSliderClick listener){
        this.data = data;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public SliderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_slider , parent , false);
        return new SliderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SliderViewHolder holder, int position) {
        MovieResults movieResults = data.get(position);
        if (movieResults.getFirst_air_date() != null){
            holder.textView.setText(movieResults.getName());
        }else {
            holder.textView.setText(movieResults.getTitle());
        }

        String imgUrl = Constants.ImgUrls.POSTER_PATH + movieResults.getBackdrop_path();
        Glide
                .with(context)
                .load(imgUrl)
                .placeholder(R.drawable.ic_movie_placeholder)
                .error(R.drawable.ic_movie_placeholder)
                .centerCrop()
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class SliderViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textView;

        public SliderViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.feutured_video_item_backdropImage);
            textView = itemView.findViewById(R.id.feutured_video_item_title);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onVideoClick(data.get(getAdapterPosition()));
                }
            });

        }
    }
}
