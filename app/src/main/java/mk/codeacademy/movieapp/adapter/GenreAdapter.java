package mk.codeacademy.movieapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.model.movie_details.Genre;

public class GenreAdapter extends RecyclerView.Adapter<GenreAdapter.GenreViewHolder> {

    ArrayList<Genre> data;
    LayoutInflater inflater;

    public GenreAdapter(Context context , ArrayList<Genre> data){
        this.data = data;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public GenreViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_genre , parent , false);
        return new GenreViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GenreViewHolder holder, int position) {
        Genre genre = data.get(position);
        holder.genre_name.setText(genre.getName());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class GenreViewHolder extends RecyclerView.ViewHolder {

        TextView genre_name;

        public GenreViewHolder(@NonNull View itemView) {
            super(itemView);

            genre_name = itemView.findViewById(R.id.genre_name);
        }
    }
}
