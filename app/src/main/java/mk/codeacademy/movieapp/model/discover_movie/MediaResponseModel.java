package mk.codeacademy.movieapp.model.discover_movie;

import java.util.ArrayList;

public class MediaResponseModel {

    int page;
    int total_results;
    int total_pages;
    ArrayList<MovieResults> results;
    String sectionTitle;
    int sectionView;
    int backgroundType;
    int sectionType;

    public MediaResponseModel(){}

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotal_results() {
        return total_results;
    }

    public void setTotal_results(int total_results) {
        this.total_results = total_results;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public ArrayList<MovieResults> getResults() {
        return results;
    }

    public void setResults(ArrayList<MovieResults> results) {
        this.results = results;
    }

    public String getSectionTitle() {
        return sectionTitle;
    }

    public void setSectionTitle(String sectionTitle) {
        this.sectionTitle = sectionTitle;
    }

    public int getSectionView() {
        return sectionView;
    }

    public void setSectionView(int sectionView) {
        this.sectionView = sectionView;
    }

    public int getBackgroundType() {
        return backgroundType;
    }

    public void setBackgroundType(int backgroundType) {
        this.backgroundType = backgroundType;
    }

    public int getSectionType() {
        return sectionType;
    }

    public void setSectionType(int sectionType) {
        this.sectionType = sectionType;
    }
}
