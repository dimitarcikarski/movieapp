package mk.codeacademy.movieapp.model.pictures;

public class Backdrop {

    String file_path;

    public Backdrop(){}

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }
}
