package mk.codeacademy.movieapp.model.videos;

import java.util.ArrayList;

public class VideosResponseModel {
    int id;
    ArrayList<VideoResults> results;

    public  VideosResponseModel(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<VideoResults> getResults() {
        return results;
    }

    public void setResults(ArrayList<VideoResults> results) {
        this.results = results;
    }
}
