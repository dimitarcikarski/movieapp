package mk.codeacademy.movieapp.model.pictures;

import java.util.ArrayList;

public class PictureResponseModel {

    int id;
    ArrayList<Backdrop> backdrops;
    ArrayList<Backdrop> posters;

    public PictureResponseModel(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Backdrop> getBackdrops() {
        return backdrops;
    }

    public void setBackdrops(ArrayList<Backdrop> backdrops) {
        this.backdrops = backdrops;
    }

    public ArrayList<Backdrop> getPosters() {
        return posters;
    }

    public void setPosters(ArrayList<Backdrop> posters) {
        this.posters = posters;
    }
}
