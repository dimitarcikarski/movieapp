package mk.codeacademy.movieapp.model.db_models;

import com.orm.SugarRecord;

public class RecentSearch extends SugarRecord<RecentSearch> {

    int movie_id;
    String type_of_picture;
    String title;

    public RecentSearch(){}

    public RecentSearch(int movie_id,String title , String type_of_picture){
        this.movie_id = movie_id;
        this.title = title;
        this.type_of_picture = type_of_picture;
    }

    public int getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(int movie_id) {
        this.movie_id = movie_id;
    }

    public String getType_of_picture() {
        return type_of_picture;
    }

    public void setType_of_picture(String type_of_picture) {
        this.type_of_picture = type_of_picture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
