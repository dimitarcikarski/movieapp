package mk.codeacademy.movieapp.model.pictures;

import java.util.ArrayList;

public class PeopleImagesResponseModel {

    ArrayList<Backdrop> profiles;
    int id;

    public PeopleImagesResponseModel(){}

    public ArrayList<Backdrop> getProfiles() {
        return profiles;
    }

    public void setProfiles(ArrayList<Backdrop> profiles) {
        this.profiles = profiles;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
