package mk.codeacademy.movieapp.model.movie_credits;

import java.util.ArrayList;

public class KnowsForResponseModel {

    int id;
    ArrayList<KnownForCast> cast;
    ArrayList<KnownForCast> crew;

    public KnowsForResponseModel(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<KnownForCast> getCast() {
        return cast;
    }

    public void setCast(ArrayList<KnownForCast> cast) {
        this.cast = cast;
    }

    public ArrayList<KnownForCast> getCrew() {
        return crew;
    }

    public void setCrew(ArrayList<KnownForCast> crew) {
        this.crew = crew;
    }
}
