package mk.codeacademy.movieapp.model.movie_credits;

import java.util.ArrayList;

public class MovieCreditsResponseModel {

    int id;
    ArrayList<Cast> cast;
    ArrayList<Crew> crew;

    public MovieCreditsResponseModel(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<Cast> getCast() {
        return cast;
    }

    public void setCast(ArrayList<Cast> cast) {
        this.cast = cast;
    }

    public ArrayList<Crew> getCrew() {
        return crew;
    }

    public void setCrew(ArrayList<Crew> crew) {
        this.crew = crew;
    }
}
