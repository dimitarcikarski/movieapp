package mk.codeacademy.movieapp.model.genres;

import java.util.ArrayList;

import mk.codeacademy.movieapp.model.movie_details.Genre;

public class GenreResponseModel {

    ArrayList<Genre> genres;

    public ArrayList<Genre> getGenres() {
        return genres;
    }
}
