package mk.codeacademy.movieapp.model.avatar;

public class Avatar {

    String name;
    String profile_path;

    public Avatar(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_path() {
        return profile_path;
    }

    public void setProfile_path(String profile_path) {
        this.profile_path = profile_path;
    }
}
