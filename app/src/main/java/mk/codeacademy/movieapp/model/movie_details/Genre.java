package mk.codeacademy.movieapp.model.movie_details;

public class Genre {

    int id;
    String name;

    public Genre(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
