package mk.codeacademy.movieapp.model.people;

import java.util.ArrayList;

import mk.codeacademy.movieapp.model.discover_movie.MovieResults;

public class PeopleResult {

    float popularity;
    String known_for_department;
    int gender;
    int id;
    String profile_path;
    boolean adult;
    ArrayList<MovieResults> known_for;
    String name;

    public float getPopularity() {
        return popularity;
    }

    public void setPopularity(float popularity) {
        this.popularity = popularity;
    }

    public String getKnown_for_department() {
        return known_for_department;
    }

    public void setKnown_for_department(String known_for_department) {
        this.known_for_department = known_for_department;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProfile_path() {
        return profile_path;
    }

    public void setProfile_path(String profile_path) {
        this.profile_path = profile_path;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public ArrayList<MovieResults> getKnown_for() {
        return known_for;
    }

    public void setKnown_for(ArrayList<MovieResults> known_for) {
        this.known_for = known_for;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
