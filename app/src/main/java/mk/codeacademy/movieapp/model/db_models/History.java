package mk.codeacademy.movieapp.model.db_models;

import com.orm.SugarRecord;

public class History extends SugarRecord<History> {

    int movie_id;
    int media_Type;
    String title;
    String poster_path;

    public History(){}

    public History(int movie_id,String title ,String poster_path , int media_Type){
        this.movie_id = movie_id;
        this.title = title;
        this.poster_path = poster_path;
        this.media_Type = media_Type;
    }

    public int getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(int movie_id) {
        this.movie_id = movie_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public int getMedia_Type() {
        return media_Type;
    }

    public void setMedia_Type(int media_Type) {
        this.media_Type = media_Type;
    }
}
