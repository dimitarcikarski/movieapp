package mk.codeacademy.movieapp.model.reviews;

import java.util.ArrayList;

public class ReviewResponseModel {

    int page;
    int total_results;
    int total_pages;
    ArrayList<ReviewResults> results;
    int id;

    public ReviewResponseModel(){}

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotal_results() {
        return total_results;
    }

    public void setTotal_results(int total_results) {
        this.total_results = total_results;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public ArrayList<ReviewResults> getResults() {
        return results;
    }

    public void setResults(ArrayList<ReviewResults> results) {
        this.results = results;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
