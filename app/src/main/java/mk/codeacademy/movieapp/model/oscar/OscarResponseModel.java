package mk.codeacademy.movieapp.model.oscar;

import java.util.ArrayList;

import mk.codeacademy.movieapp.model.discover_movie.MovieResults;

public class OscarResponseModel {

    ArrayList<MovieResults> items;


    public OscarResponseModel(){}

    public ArrayList<MovieResults> getItems() {
        return items;
    }

    public void setItems(ArrayList<MovieResults> items) {
        this.items = items;
    }
}
