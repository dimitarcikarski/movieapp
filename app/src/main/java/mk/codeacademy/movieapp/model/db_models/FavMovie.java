package mk.codeacademy.movieapp.model.db_models;

import com.orm.SugarRecord;

public class FavMovie extends SugarRecord<FavMovie> {

    int movie_id;
    int type_of_picture;
    String title;
    String poster_path;

    public FavMovie(){}

    public FavMovie(int movie_id,String title ,String poster_path , int type_of_picture){
        this.movie_id = movie_id;
        this.title = title;
        this.poster_path = poster_path;
        this.type_of_picture = type_of_picture;
    }

    public int getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(int movie_id) {
        this.movie_id = movie_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public int getType_of_picture() {
        return type_of_picture;
    }

    public void setType_of_picture(int type_of_picture) {
        this.type_of_picture = type_of_picture;
    }
}
