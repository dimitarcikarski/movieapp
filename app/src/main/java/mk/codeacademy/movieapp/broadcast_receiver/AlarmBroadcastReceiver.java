package mk.codeacademy.movieapp.broadcast_receiver;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.core.app.NotificationCompat;

import mk.codeacademy.movieapp.MainActivity;
import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.interfaces.TransferNotification;

public class AlarmBroadcastReceiver extends BroadcastReceiver implements TransferNotification {

    public static final String CHANNEL_ID_ = "CHANNEL_ID_";

    String mtext;

    @Override
    public void onReceive(Context context, Intent intent) {
        showNotification(context);
    }

    void showNotification(Context context) {
        String CHANNEL_ID = CHANNEL_ID_;
        CharSequence name = context.getResources().getString(R.string.app_name);
        NotificationCompat.Builder mBuilder;
        Intent notificationIntent = new Intent(context, MainActivity.class);
        Bundle bundle = new Bundle();

        notificationIntent.putExtras(bundle);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= 26) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_HIGH);
            mNotificationManager.createNotificationChannel(mChannel);
            mBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLights(Color.WHITE, 300, 300)
                    .setChannelId(CHANNEL_ID)
                    .setContentTitle("Featured Movies");
        } else {
            mBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setContentTitle("Featured Movies");
        }

        mBuilder.setContentIntent(contentIntent);
        mBuilder.setContentText("Explore Movies, TV-Shows and People on Movie App");
        mBuilder.setAutoCancel(true);
        mNotificationManager.notify(1, mBuilder.build());
    }

    @Override
    public void transferNotification(String text) {
        mtext = text;
    }
}