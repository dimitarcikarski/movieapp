package mk.codeacademy.movieapp.common;

public class Constants {

    public class API_key{

        public static final String API_KEY = "261eb855ea119759e11756f84891327f";
    }

    public class ImgUrls {
        public static final String POSTER_PATH = "https://image.tmdb.org/t/p/w500";
        public static final String YOUTUBE_THUMBNAIL = "https://img.youtube.com/vi/";
        public static final String YOUTUBE_THUMBNAIL_QUALITY = "/mqdefault.jpg";
    }

    public class Genres{

        public static final int ACTION =  28;

        public static final int ANIMATED =  16;

        public static final int DOCUMENTARY = 99;

        public static final int DRAMA = 18;

        public static final int FAMILY = 10751;

        public static final int FANTASY = 14;

        public static final int HISTORY = 36;

        public static final int COMEDY = 35;

        public static final int WAR = 10752;

        public static final int CRIME = 80;

        public static final int MUSIC = 10402;

        public static final int MYSTERY = 9648;

        public static final int ROMANCE = 10749;

        public static final int SCI_FI = 878;

        public static final int HORROR = 27;

        public static final int TV_MOVIE = 10770;

        public static final int THRILLER = 53;

        public static final int WESTERN = 37;

        public static final int ADVENTURE = 12;
    }

    public class SectionTitle{

        public static final int RECOMMENDED_MOVIES = 0;

        public static final int RECOMMENDED_TV = 1;

        public static final int FEATURED_LISTS = 2;

        public static final int ON_TV = 3;

        public static final int IN_TEATHERS = 4;
    }

    public class MovieSectionType{

        public static final int IN_THEATHERS = 0;

        public static final int POPULAR = 1;

        public static final int UPCOMMING = 2;

        public static final int TOP_RATED = 3;

    }

    public class TvSectionType{

        public static final int AIRING_TODAY = 0;

        public static final int POPULAR = 1;

        public static final int ON_THE_AIR = 2;

        public static final int TOP_RATED = 3;

    }

    public class TypeOfPicture{

        public static final int MOVIE = 0;

        public static final int TV = 1;

        public static final int PERSON= 2;

        public static final String MOVIE_STRING = "movie";

        public static final String TV_STRING = "tv";

        public static final int GENRE= 3;


    }

    public class TypeOfView{

        public static final int BIG_VIEW = 100;

        public static final int NORMAL_VIEW = 200;

        public static final int NORMAL_VIEW_SPAN_TWO = 300;
    }

    public class TypeOfSection{

        public static final int TOP_RATED = 1;

        public static final int SIMILAR = 20;

        public static final int NORMAL = 3;
    }

    public class BackgroundType{

        public static final int WHITE = 100;

        public static final int BLACK = 200;
    }

    public class ExtrasKey{

        public static final String ID = "movie";

        public static final String CONTENT_CHECK = "CHECK";

        public static final String VIDEO_POSITION = "POSITION";

        public static final String SECTION_TYPE = "SECTION_TYPE";

        public static final String GENRE_TYPE = "Genre_TYPE";

        public static final String GENRE_TYPE_ID = "Genre_TYPE_ID";

        public static final String GENRE_TYPE_NAME = "Genre_TYPE_NAME";

        public static final String MEDIA_TYPE = "MEDIA_TYPE";

        public static final String TYPE_OF_FAV = "TYPE_OF_FAV";

        public static final String ON_BOARDING_KEY = "ON_BOARDING_KEY";
    }

    public class Type_Of_DB{

        public static final int FAV_MOVIE = 1;

        public static final int FAV_TV = 2;

        public static final int WATCHLIST = 3;
    }


    public class Media_Type{

        public static final String MOVIE = "movie";

        public static final String TV = "tv";

        public static final String PERSON = "person";
    }
}
