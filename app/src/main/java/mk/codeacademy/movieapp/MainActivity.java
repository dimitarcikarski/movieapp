package mk.codeacademy.movieapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Calendar;

import mk.codeacademy.movieapp.activity.SearchActivity;
import mk.codeacademy.movieapp.broadcast_receiver.AlarmBroadcastReceiver;
import mk.codeacademy.movieapp.fragment.ProfileFragment;
import mk.codeacademy.movieapp.fragment.HomeFragment;
import mk.codeacademy.movieapp.fragment.MovieFragment;
import mk.codeacademy.movieapp.fragment.PeopleFragment;
import mk.codeacademy.movieapp.fragment.TVFragment;
import mk.codeacademy.movieapp.fragment.FavouritesFragment;
import mk.codeacademy.movieapp.helper.QueryBuilder;
import mk.codeacademy.movieapp.helper.Requests;
import mk.codeacademy.movieapp.interfaces.TransferNotification;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    BottomNavigationView bottomNav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        findViewById(R.id.toolbar).bringToFront();

        bottomNav = findViewById(R.id.bottomNav);
        bottomNav.setOnNavigationItemSelectedListener(this);
        bottomNav.setSelectedItemId(R.id.nav_home);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        switch (menuItem.getItemId()){
            case R.id.nav_home:
                ft.replace(R.id.fragment_container, HomeFragment.newInstance() , HomeFragment.TAG);
                ft.addToBackStack(HomeFragment.TAG);
                ft.commit();
                return true;
            case R.id.nav_movie:
                ft.replace(R.id.fragment_container, MovieFragment.newInstance(), MovieFragment.TAG);
                ft.addToBackStack(MovieFragment.TAG);
                ft.commit();
                return true;
            case R.id.nav_tv:
                ft.replace(R.id.fragment_container, TVFragment.newInstance(), TVFragment.TAG);
                ft.addToBackStack(TVFragment.TAG);
                ft.commit();
                return true;
            case R.id.nav_people:
                ft.replace(R.id.fragment_container, PeopleFragment.newInstance(), PeopleFragment.TAG);
                ft.addToBackStack(PeopleFragment.TAG);
                ft.commit();
                return true;
            case R.id.nav_favourite:
                ft.replace(R.id.fragment_container, ProfileFragment.newInstance(), FavouritesFragment.TAG);
                ft.addToBackStack(ProfileFragment.TAG);
                ft.commit();
                return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Fragment fragment = this.getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof HomeFragment){
            bottomNav.setSelectedItemId(R.id.nav_home);
        }else if (fragment instanceof MovieFragment){
            bottomNav.setSelectedItemId(R.id.nav_movie);
        }else if (fragment instanceof TVFragment){
            bottomNav.setSelectedItemId(R.id.nav_tv);
        }else if (fragment instanceof PeopleFragment){
            bottomNav.setSelectedItemId(R.id.nav_people);
        }else if (fragment instanceof FavouritesFragment){
            bottomNav.setSelectedItemId(R.id.nav_favourite);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.search:
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
                return true;
        }
        return false;
    }
}
