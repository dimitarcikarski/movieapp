package mk.codeacademy.movieapp.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import mk.codeacademy.movieapp.R;

public class BoardingFragmentTwo extends Fragment {

    public static final String TAG = BoardingFragmentTwo.class.getSimpleName();

    public static BoardingFragmentTwo newInstance() {

        Bundle args = new Bundle();

        BoardingFragmentTwo fragment = new BoardingFragmentTwo();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_boarding_fragment_two, container, false);
    }

}
