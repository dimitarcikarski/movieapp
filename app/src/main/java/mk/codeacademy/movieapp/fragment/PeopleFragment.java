package mk.codeacademy.movieapp.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.util.ArrayList;

import mk.codeacademy.movieapp.activity.ActorDetailActivity;
import mk.codeacademy.movieapp.helper.QueryBuilder;
import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.helper.Requests;
import mk.codeacademy.movieapp.adapter.PeopleAdapter;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnPersonClick;
import mk.codeacademy.movieapp.model.people.PeopleResult;

public class PeopleFragment extends Fragment implements OnPersonClick {

    public static final String TAG = PeopleFragment.class.getSimpleName();

    LinearLayout linearLayoutPeople;
    ProgressBar progressBar;
    RecyclerView peopleRecyclerView;
    ArrayList<PeopleResult> peopleDatalist = new ArrayList<>();
    OnPersonClick onPersonClick;
    PeopleAdapter adapter;
    Requests requests = new Requests();
    QueryBuilder queryBuilder = new QueryBuilder();

    boolean isloading = false;
    int page = 1;

    public PeopleFragment() {}

    public static PeopleFragment newInstance() {

        Bundle args = new Bundle();

        PeopleFragment fragment = new PeopleFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_people, container, false);
        linearLayoutPeople = view.findViewById(R.id.linearLayoutPeople);
        progressBar = view.findViewById(R.id.people_pb);

        Requests requests = new Requests();
        QueryBuilder queryBuilder = new QueryBuilder();
        peopleDatalist = new ArrayList<>();
        peopleRecyclerView = view.findViewById(R.id.people_recyclerView);
        onPersonClick = this;

        peopleRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3, LinearLayoutManager.VERTICAL, false));
        adapter = new PeopleAdapter(getActivity() , peopleDatalist , this);
        peopleRecyclerView.setAdapter(adapter);
        ViewCompat.setNestedScrollingEnabled(peopleRecyclerView , false);

        String peopleUrl  = queryBuilder.people(page+"");

        requests.people(linearLayoutPeople ,progressBar , peopleDatalist ,adapter, getActivity() , peopleUrl);

        initScrollListener();

        return view;
    }


    @Override
    public void onPersonClick(PeopleResult peopleResult) {
        Intent intent = new Intent(getActivity(), ActorDetailActivity.class);
        intent.putExtra(Constants.ExtrasKey.ID , peopleResult.getId());
        startActivity(intent);
    }

    private void initScrollListener(){
        peopleRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (!isloading){
                    if(linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == peopleDatalist.size() - 1){
                        int nextPage = page + 1;
                        if (page <= 11) {
                            loadMore(nextPage);
                        }
                        isloading = true;
                    }
                }
            }
        });
    }

    private void loadMore(final int nextPage) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                String peopleUrl  = queryBuilder.people(nextPage+"");
                requests.people(linearLayoutPeople ,progressBar , peopleDatalist ,adapter, getActivity() , peopleUrl);
                page++;
                isloading = false;
            }
        } , 100);
    }
}
