package mk.codeacademy.movieapp.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import mk.codeacademy.movieapp.activity.DetailActivity;
import mk.codeacademy.movieapp.helper.QueryBuilder;
import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.helper.Requests;
import mk.codeacademy.movieapp.activity.SeeAllActivity;
import mk.codeacademy.movieapp.activity.VideoActivity;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnGenreClick;
import mk.codeacademy.movieapp.interfaces.OnMediaItemClick;
import mk.codeacademy.movieapp.interfaces.OnSliderClick;
import mk.codeacademy.movieapp.model.discover_movie.MediaResponseModel;
import mk.codeacademy.movieapp.model.discover_movie.MovieResults;
import mk.codeacademy.movieapp.model.movie_credits.KnownForCast;
import mk.codeacademy.movieapp.model.movie_details.Genre;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Response;

import static android.content.Context.MODE_PRIVATE;


public class HomeFragment extends Fragment implements OnMediaItemClick , OnSliderClick , OnGenreClick{

    public static final String TAG = HomeFragment.class.getSimpleName();

    LinearLayout linearLayout;
    ProgressBar progressBar;
    ProgressBar randomProgressBar;
    RecyclerView recyclerView;
    RecyclerView sliderRecyclerView;
    RecyclerView oscar_winners_recyclerView;
    ArrayList<MediaResponseModel> datalist = new ArrayList<>();
    ArrayList<MovieResults> sliderData = new ArrayList<>();
    ArrayList<MovieResults> randomData = new ArrayList<>();
    ArrayList<MovieResults> oscarsDataList = new ArrayList<>();
    ArrayList<Genre> genreArrayList = new ArrayList<>();
    RecyclerView frontgenreRV;

    SharedPreferences sharedPreferences;
    String page = "1";
    String favUrl;
    Gson gson;

    TextView randomizer_title;
    TextView randomizer_movie_title;
    Button randomizer_dice;
    Button once_again;
    ImageView randomizer_poster;
    TextView click_here;
    int pageFav = 1;

    Requests requests = new Requests();
    QueryBuilder queryBuilder = new QueryBuilder();

    MovieResults randomResult;

    public static HomeFragment newInstance() {

        Bundle args = new Bundle();

        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        linearLayout = view.findViewById(R.id.linear_home);
        progressBar = view.findViewById(R.id.home_pb);

        randomizer_title = view.findViewById(R.id.randomizer_title_ofc);
        randomizer_movie_title= view.findViewById(R.id.randomizer_movie_title);
        randomizer_dice= view.findViewById(R.id.dice_button);
        once_again = view.findViewById(R.id.once_again);
        randomizer_poster = view.findViewById(R.id.randomize_poster);
        click_here = view.findViewById(R.id.click_here);
        randomProgressBar = view.findViewById(R.id.random_progressBar);
        oscar_winners_recyclerView = view.findViewById(R.id.oscar_winners_recyclerView);

        gson = new Gson();

        datalist = new ArrayList<>();
        sliderData = new ArrayList<>();
        recyclerView = view.findViewById(R.id.feautured_recyclerView);
        sliderRecyclerView = view.findViewById(R.id.feautured_slider_recyclerView);
        frontgenreRV = view.findViewById(R.id.front_genres_recyclerView);

        sharedPreferences = getActivity().getSharedPreferences("MY_SHARED_PREF",MODE_PRIVATE);

        String popularMoviesUrl = queryBuilder.movies(Constants.MovieSectionType.POPULAR , page);

        favUrl = sharedPreferences.getString("genre1" , null)
                + sharedPreferences.getString("genre2" , null)
                +sharedPreferences.getString("genre3" , null);

        String favouriteGenreUrl = queryBuilder.moviesByFavouriteGenre(favUrl , pageFav);
        String trending = queryBuilder.trending();
        String topRatedUrl = queryBuilder.tvShows(Constants.TvSectionType.TOP_RATED , page);
        String TVpopularUrl = queryBuilder.tvShows(Constants.TvSectionType.POPULAR , page);
        String frontGenreUrl = queryBuilder.frontGenre();
        String oscarWinnersUrl = queryBuilder.oscarWinners();

        requests.loadFeaturedVideos(sliderData , sliderRecyclerView , getActivity() , trending , this);

        requests.loadMoviesTvs(
                linearLayout ,
                progressBar ,
                recyclerView ,
                datalist ,
                getActivity() ,
                Constants.TypeOfView.BIG_VIEW,
                popularMoviesUrl ,
                "Popular Movies",
                this,
                Constants.BackgroundType.WHITE,
                Constants.MovieSectionType.POPULAR ,
                Constants.TypeOfPicture.MOVIE );

        requests.loadMoviesTvs(linearLayout ,
                progressBar ,
                recyclerView ,
                datalist ,
                getActivity() ,
                Constants.TypeOfView.NORMAL_VIEW,
                TVpopularUrl , "Popular TV-Shows" ,
                this,
                Constants.BackgroundType.WHITE,
                Constants.TvSectionType.POPULAR ,
                Constants.TypeOfPicture.TV);



        requests.front_genres(frontgenreRV , genreArrayList , getActivity() , this, frontGenreUrl );
        requests.loadOscarWinners(oscarsDataList , oscar_winners_recyclerView , getActivity() , oscarWinnersUrl , this);


        once_again.setVisibility(View.GONE);
        randomizer_poster.setVisibility(View.GONE);
        randomizer_movie_title.setVisibility(View.GONE);
        randomProgressBar.setVisibility(View.GONE);

        randomizer_dice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                randomProgressBar.setVisibility(View.VISIBLE);
                randomize();
            }
        });
        once_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                randomProgressBar.setVisibility(View.VISIBLE);
                randomize();
            }
        });
        return view;
    }


    @Override
    public void onItemClick(MovieResults movie) {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(Constants.ExtrasKey.ID , movie.getId());
        intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , movie.getFirst_air_date());
        startActivity(intent);
    }

    @Override
    public void onKnownForClick(KnownForCast knownForCast) {

    }

    @Override
    public void onSeeAllClick(MediaResponseModel mediaResponseModel, int mediaType) {
        Intent intent = new Intent(getActivity(), SeeAllActivity.class);
        intent.putExtra(Constants.ExtrasKey.SECTION_TYPE , mediaResponseModel.getSectionType());
        intent.putExtra(Constants.ExtrasKey.MEDIA_TYPE , mediaType);
        startActivity(intent);
    }

    @Override
    public void onVideoClick(MovieResults movieResults) {
        Intent intent = new Intent(getActivity(), VideoActivity.class);
        intent.putExtra(Constants.ExtrasKey.ID , movieResults.getId());
        intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , movieResults.getFirst_air_date());
        intent.putExtra(Constants.ExtrasKey.VIDEO_POSITION , 0);
        startActivity(intent);
    }

    @Override
    public void onGenreClick(Genre genre) {
        Intent intent = new Intent(getActivity(), SeeAllActivity.class);
        intent.putExtra(Constants.ExtrasKey.MEDIA_TYPE, Constants.TypeOfPicture.GENRE);
        intent.putExtra(Constants.ExtrasKey.GENRE_TYPE_NAME , genre.getName());
        intent.putExtra(Constants.ExtrasKey.GENRE_TYPE_ID , genre.getId());
        startActivity(intent);
    }

    void randomize(){
        QueryBuilder queryBuilder = new QueryBuilder();
        int i;
        for ( i = 1 ; i<6 ; i++) {
            String url = queryBuilder.moviesByFavouriteGenre(favUrl, i);
            OkHttpClient client = new OkHttpClient();

            final okhttp3.Request request = new okhttp3.Request.Builder().url(url).build();
            final int finalI = i;
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException e) {
                    e.getStackTrace();
                }

                @Override
                public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                    if (response.isSuccessful()) {
                        String jsonString = response.body().string();
                        final MediaResponseModel mediaResponseModel = gson.fromJson(jsonString, MediaResponseModel.class);
                        final ArrayList<MovieResults> helperList = mediaResponseModel.getResults();
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                randomData.addAll(helperList);
                                if (finalI == 5){
                                    Random  random = new Random();
                                    int k = random.nextInt(randomData.size()+1);
                                    randomResult = randomData.get(k);
                                    randomProgressBar.setVisibility(View.GONE);
                                    once_again.setVisibility(View.VISIBLE);
                                    randomizer_poster.setVisibility(View.VISIBLE);
                                    randomizer_movie_title.setVisibility(View.VISIBLE);
                                    String imgUrl = Constants.ImgUrls.POSTER_PATH + randomResult.getPoster_path();
                                    Glide
                                            .with(getActivity())
                                            .load(imgUrl)
                                            .placeholder(R.drawable.ic_movie_placeholder)
                                            .error(R.drawable.ic_movie_placeholder)
                                            .centerCrop()
                                            .into(randomizer_poster);

                                    randomizer_movie_title.setText(randomResult.getTitle());
                                    randomizer_dice.setVisibility(View.GONE);
                                    randomizer_title.setVisibility(View.GONE);
                                    click_here.setVisibility(View.GONE);
                                    once_again.setVisibility(View.VISIBLE);


                                    randomizer_poster.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Intent(getActivity(), DetailActivity.class);
                                            intent.putExtra(Constants.ExtrasKey.ID , randomResult.getId());
                                            intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , randomResult.getFirst_air_date());
                                            startActivity(intent);
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            });
        }
    }

}
