package mk.codeacademy.movieapp.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mk.codeacademy.movieapp.activity.DetailActivity;
import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.adapter.FavMovieAdapter;
import mk.codeacademy.movieapp.adapter.FavTvShowAdapter;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnFavMovieClick;
import mk.codeacademy.movieapp.model.db_models.FavMovie;
import mk.codeacademy.movieapp.model.db_models.FavTvShows;

public class FavouritesFragment extends Fragment implements OnFavMovieClick {

    public static final String TAG = FavouritesFragment.class.getSimpleName();
    RecyclerView favRecyclerView;
    List<FavMovie> favMovies = new ArrayList<>();
    List<FavTvShows> favTvShows = new ArrayList<>();
    FavTvShowAdapter tvShowAdapter;
    FavMovieAdapter favMovieAdapter;
    int typeOfDB;
    TextView noMovieTv;

    public static FavouritesFragment newInstance(int typeOfDB) {
        Bundle args = new Bundle();
        args.putInt(Constants.ExtrasKey.TYPE_OF_FAV , typeOfDB);
        FavouritesFragment fragment = new FavouritesFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_watched, container, false);

        noMovieTv = view.findViewById(R.id.noMovieTV);

        if (getArguments() != null) {
            typeOfDB = getArguments().getInt(Constants.ExtrasKey.TYPE_OF_FAV , -1);
        }

        favRecyclerView = view.findViewById(R.id.fav_recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL, false);
        favRecyclerView.setLayoutManager(linearLayoutManager);

        loadDatabases();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadDatabases();

    }

    public void loadDatabases(){
        if (typeOfDB == Constants.Type_Of_DB.FAV_MOVIE){
            favMovies = new ArrayList<>();
            favMovies = FavMovie.listAll(FavMovie.class);
            if (favMovies.size()>0){
                noMovieTv.setVisibility(View.GONE);
            }else {
                noMovieTv.setVisibility(View.VISIBLE);
                noMovieTv.setText("No movies added yet.");
            }
            Collections.reverse(favMovies);
            favMovieAdapter = new FavMovieAdapter(getActivity() , favMovies , this );
            favRecyclerView.setAdapter(favMovieAdapter);

        }else if (typeOfDB == Constants.Type_Of_DB.FAV_TV) {
            favTvShows = new ArrayList<>();
            favTvShows = FavTvShows.listAll(FavTvShows.class);
            if (favTvShows.size()>0){
                noMovieTv.setVisibility(View.GONE);
            }else {
                noMovieTv.setVisibility(View.VISIBLE);
                noMovieTv.setText("No TV-Shows added yet.");
            }
            Collections.reverse(favTvShows);
            tvShowAdapter = new FavTvShowAdapter(getActivity(), favTvShows, this);
            favRecyclerView.setAdapter(tvShowAdapter);
        }
    }

    @Override
    public void onMovieClick(FavMovie favMovie) {
        String type = null;
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(Constants.ExtrasKey.ID , favMovie.getMovie_id());
        intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , type);
        startActivity(intent);
    }

    @Override
    public void onTvClick(FavTvShows tvShows) {
        String type = Constants.Media_Type.TV;
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(Constants.ExtrasKey.ID , tvShows.getMovie_id());
        intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , type);
        startActivity(intent);
    }
}
