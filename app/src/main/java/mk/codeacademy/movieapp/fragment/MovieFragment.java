package mk.codeacademy.movieapp.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.util.ArrayList;

import mk.codeacademy.movieapp.activity.DetailActivity;
import mk.codeacademy.movieapp.helper.QueryBuilder;
import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.helper.Requests;
import mk.codeacademy.movieapp.activity.SeeAllActivity;
import mk.codeacademy.movieapp.activity.VideoActivity;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnMediaItemClick;
import mk.codeacademy.movieapp.interfaces.OnSliderClick;
import mk.codeacademy.movieapp.model.discover_movie.MediaResponseModel;
import mk.codeacademy.movieapp.model.discover_movie.MovieResults;
import mk.codeacademy.movieapp.model.movie_credits.KnownForCast;

public class MovieFragment extends Fragment implements OnMediaItemClick , OnSliderClick {

    public static final String TAG = MovieFragment.class.getSimpleName();

    LinearLayout linearLayout;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    RecyclerView sliderRecyclerView;
    ArrayList<MediaResponseModel> datalist = new ArrayList<>();
    ArrayList<MovieResults> sliderData = new ArrayList<>();
    RecyclerView topRatedMovie_recyclerView;
    ArrayList<MediaResponseModel> topRateddatalist = new ArrayList<>();
    String page = "1";

    public MovieFragment() {}

    public static MovieFragment newInstance() {

        Bundle args = new Bundle();

        MovieFragment fragment = new MovieFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie, container, false);
        linearLayout = view.findViewById(R.id.linear_movies);
        progressBar = view.findViewById(R.id.movies_pb);

        Requests requests = new Requests();
        QueryBuilder queryBuilder = new QueryBuilder();
        datalist = new ArrayList<>();
        sliderData = new ArrayList<>();
        recyclerView = view.findViewById(R.id.movie_recyclerView);
        sliderRecyclerView = view.findViewById(R.id.movie_slider_recyclerView);
        topRatedMovie_recyclerView = view.findViewById(R.id.topRatedMovie_recyclerView);

        String inTeathersUrl = queryBuilder.movies(Constants.MovieSectionType.IN_THEATHERS , page);
        String popularUrl = queryBuilder.movies(Constants.MovieSectionType.POPULAR , page);
        String upcomingUrl = queryBuilder.movies(Constants.MovieSectionType.UPCOMMING , page);
        String topRatedUrl = queryBuilder.movies(Constants.MovieSectionType.TOP_RATED , page);

        requests.loadFeaturedVideos(sliderData , sliderRecyclerView , getActivity() , inTeathersUrl , this);

        requests.loadMoviesTvs(linearLayout , progressBar ,recyclerView ,datalist , getActivity() ,Constants.TypeOfView.NORMAL_VIEW, popularUrl , "Popular" , this, Constants.BackgroundType.WHITE,Constants.MovieSectionType.POPULAR ,Constants.TypeOfPicture.MOVIE );
        requests.loadMoviesTvs(linearLayout , progressBar ,recyclerView ,datalist , getActivity() ,Constants.TypeOfView.BIG_VIEW, upcomingUrl , "Upcoming", this, Constants.BackgroundType.WHITE,Constants.MovieSectionType.UPCOMMING , Constants.TypeOfPicture.MOVIE );
        requests.loadMoviesTvs(linearLayout , progressBar ,topRatedMovie_recyclerView ,topRateddatalist , getActivity() ,Constants.TypeOfView.NORMAL_VIEW, topRatedUrl , "Top Rated", this, Constants.BackgroundType.BLACK,Constants.MovieSectionType.TOP_RATED ,Constants.TypeOfPicture.MOVIE );

        return view;
    }

    @Override
    public void onItemClick(MovieResults movie) {
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(Constants.ExtrasKey.ID , movie.getId());
        intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , movie.getFirst_air_date());
        startActivity(intent);
    }

    @Override
    public void onKnownForClick(KnownForCast knownForCast) {

    }

    @Override
    public void onSeeAllClick(MediaResponseModel mediaResponseModel, int mediaType) {
        Intent intent = new Intent(getActivity(), SeeAllActivity.class);
        intent.putExtra(Constants.ExtrasKey.SECTION_TYPE , mediaResponseModel.getSectionType());
        intent.putExtra(Constants.ExtrasKey.MEDIA_TYPE , mediaType);
        startActivity(intent);
    }

    @Override
    public void onVideoClick(MovieResults movieResults) {
        Intent intent = new Intent(getActivity(), VideoActivity.class);
        intent.putExtra(Constants.ExtrasKey.ID , movieResults.getId());
        intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , movieResults.getFirst_air_date());
        intent.putExtra(Constants.ExtrasKey.VIDEO_POSITION , 0);
        startActivity(intent);
    }
}
