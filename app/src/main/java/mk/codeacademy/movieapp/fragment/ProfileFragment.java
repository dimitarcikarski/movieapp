package mk.codeacademy.movieapp.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mk.codeacademy.movieapp.activity.ActorDetailActivity;
import mk.codeacademy.movieapp.activity.DetailActivity;
import mk.codeacademy.movieapp.R;
import mk.codeacademy.movieapp.adapter.FavWatchlistAdapter;
import mk.codeacademy.movieapp.adapter.ProfilePagerAdapter;
import mk.codeacademy.movieapp.adapter.RecentAdapter;
import mk.codeacademy.movieapp.common.Constants;
import mk.codeacademy.movieapp.interfaces.OnHistoryClick;
import mk.codeacademy.movieapp.interfaces.OnWatchlistClick;
import mk.codeacademy.movieapp.model.db_models.History;
import mk.codeacademy.movieapp.model.db_models.Watchlist;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements OnHistoryClick , OnWatchlistClick {

    public static final String TAG = ProfileFragment.class.getSimpleName();

    LinearLayout linearLayoutPeople;
    ProgressBar progressBar;
    ProfilePagerAdapter pagerAdapter;
    TabLayout tabLayout;
    ViewPager viewPager;
    RecyclerView recentRecyclerView;
    List<History> recentDatalist = new ArrayList<>();
    RecyclerView watchlistRecyclerview;
    List<Watchlist> watchlistDatalist = new ArrayList<>();
    RecentAdapter adapter;
    FavWatchlistAdapter favWatchlistAdapter;
    TextView recent_clear;
    TextView watchlist_clear;
    TextView noRecentBrowses;
    TextView noWatchlist;

    private void setupPagerAdapter(ViewPager viewPager){
        pagerAdapter = new ProfilePagerAdapter(getChildFragmentManager());
        pagerAdapter.addFragment(FavouritesFragment.newInstance(Constants.Type_Of_DB.FAV_MOVIE) , "Movies");
        pagerAdapter.addFragment(FavouritesFragment.newInstance(Constants.Type_Of_DB.FAV_TV), "TV");
        viewPager.setAdapter(pagerAdapter);
    }


    public static ProfileFragment newInstance() {

        Bundle args = new Bundle();
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        linearLayoutPeople = view.findViewById(R.id.linearLayout_profile);
        progressBar = view.findViewById(R.id.profile_pb);

        recent_clear = view.findViewById(R.id.recent_clear);
        watchlist_clear = view.findViewById(R.id.watchlist_clear);
        noRecentBrowses = view.findViewById(R.id.noRecentBrowses);
        noWatchlist = view.findViewById(R.id.noWatchlist);

        viewPager = view.findViewById(R.id.favourites_view_pager);
        setupPagerAdapter(viewPager);
        tabLayout = view.findViewById(R.id.favourites_tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        //TODO
        //setupIcons();
        recentRecyclerView = view.findViewById(R.id.recentRecyclerview);
        recentRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL, false));
        loadRecentHistory();

        watchlistRecyclerview = view.findViewById(R.id.watchlistRecyclerview);
        watchlistRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL, false));
        loadWatchlist();


        recent_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                History.deleteAll(History.class);
                recentDatalist.clear();
                adapter.notifyDataSetChanged();
                if (recentDatalist.size() > 0){
                    noRecentBrowses.setVisibility(View.GONE);
                }else {
                    noRecentBrowses.setVisibility(View.VISIBLE);
                }
            }
        });

        watchlist_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Watchlist.deleteAll(Watchlist.class);
                watchlistDatalist.clear();
                favWatchlistAdapter.notifyDataSetChanged();
                if (watchlistDatalist.size() > 0){
                    noWatchlist.setVisibility(View.GONE);
                }else {
                    noWatchlist.setVisibility(View.VISIBLE);
                }
            }
        });
        return view;
    }

    @Override
    public void onHistoryClick(History history) {
        String type;
        if (history.getMedia_Type() == Constants.TypeOfPicture.MOVIE){
            type = null;
            Intent intent = new Intent(getActivity(), DetailActivity.class);
            intent.putExtra(Constants.ExtrasKey.ID , history.getMovie_id());
            intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , type);
            startActivity(intent);
        }else if (history.getMedia_Type() == Constants.TypeOfPicture.TV){
            type = Constants.Media_Type.TV;
            Intent intent = new Intent(getActivity(), DetailActivity.class);
            intent.putExtra(Constants.ExtrasKey.ID , history.getMovie_id());
            intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , type);
            startActivity(intent);
        }else {
            Intent intent = new Intent(getActivity(), ActorDetailActivity.class);
            intent.putExtra(Constants.ExtrasKey.ID , history.getMovie_id());
            startActivity(intent);
        }

    }

    public void loadRecentHistory(){
        recentDatalist = new ArrayList<>();
        recentDatalist = History.listAll(History.class);
        if (recentDatalist.size() > 0){
            noRecentBrowses.setVisibility(View.GONE);
        }else {
            noRecentBrowses.setVisibility(View.VISIBLE);
        }
        Collections.reverse(recentDatalist);
        adapter = new RecentAdapter(getActivity() , recentDatalist , this);
        recentRecyclerView.setAdapter(adapter);
        progressBar.setVisibility(View.GONE);
        linearLayoutPeople.setVisibility(View.VISIBLE);
    }

    public void loadWatchlist(){
        watchlistDatalist = new ArrayList<>();
        watchlistDatalist = Watchlist.listAll(Watchlist.class);
        if (watchlistDatalist.size() > 0){
            noWatchlist.setVisibility(View.GONE);
        }else {
            noWatchlist.setVisibility(View.VISIBLE);
        }
        Collections.reverse(watchlistDatalist);
        favWatchlistAdapter = new FavWatchlistAdapter(getActivity() , watchlistDatalist, this);
        watchlistRecyclerview.setAdapter(favWatchlistAdapter);
        progressBar.setVisibility(View.GONE);
        linearLayoutPeople.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadRecentHistory();
    }

    @Override
    public void onWatchlistClick(Watchlist watchlist) {
        String type;
        if (watchlist.getType_of_picture() == Constants.TypeOfPicture.MOVIE){
            type = null;
        }else {
            type = Constants.Media_Type.TV;
        }
        Intent intent = new Intent(getActivity(), DetailActivity.class);
        intent.putExtra(Constants.ExtrasKey.ID , watchlist.getMovie_id());
        intent.putExtra(Constants.ExtrasKey.CONTENT_CHECK , type);
        startActivity(intent);
    }

    /*private void setupIcons(){
        tabLayout.getTabAt(0).setIcon(R.drawable.baseline_favorite_white_48);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_watched);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_popular_people);

    }*/



}
